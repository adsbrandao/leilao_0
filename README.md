# LEILAO
rewre

## Como instalar:

- Primeiro é necessário instalar o [composer](https://getcomposer.org) e o [SASS](https://sass-lang.com/install).
- O php instalado e em uso deve ser o PHP 7.2 ou superior.
- Após instalar o composer e o npm rode:

```
composer install
```

Caso dê erro com o npm ou sass ou babel, tente instalar manualmente cada um(como exemplos abaixo)


# Compilar SCSS para CSS

Antes deve instalar o [npm](https://www.npmjs.com/) e instalar o [SASS](https://sass-lang.com/install):

```sh
npm install -g sass
```

Saiba mais em [sass-lang.com](https://sass-lang.com)

Para compilar o css apartir do SCSS use o comando:

```sh
./buildcss.sh
```

Nota: Caso esteja no windows, use o **git bash** para rodar.

## Configurações

Para editar configurações do ambiente altere o arquivo **config.php**

## Para Testar na sua máquina

php -S localhost:8080

# USO DO CLI

## Ao alterar campos do banco de dados ou adicionar tabela rode:

```sh
php cli.php generate
```

## Para criar um arquivo do tipo Controller

```sh
php cli.php controller <NameContrller>
```

Exemplo: 

```sh
php cli.php controller Clientes
```

## Para criar um arquivo do tipo ControllerResource

```sh
php cli.php resource <NameController> <NameModel>
```

Exemplo:

```sh
php cli.php resource Clientes ClienteModel
