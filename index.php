<?php 
ob_start();
define('MODO', 'DEBUG');
include('app/bootstrap.php');

try {    
    include(PATH . 'app/routers.php');
} catch (\Exception $e) {
    echo '<h1>Ocorreu um erro!</h1>';
    echo '<p>'.$e->getMessage().'</p>';
    if (MODO == 'DEBUG') {
        echo "<pre>";
        foreach ($e->getTrace() as $k => $item) {
            echo "\r\n".$k."# ";
            echo $item['file'].":".$item['line']??'';
            echo "\r\n";
            echo $item['class']??'';
            echo $item['type']??'';
            echo $item['function']??'';
            echo isset($item['args'])? '('.json_encode($item['args']).')' : '';
            echo "\r\n";
        }
        echo "</pre>";
    }
    echo \Core\View::getTemplate();
    exit;
}
