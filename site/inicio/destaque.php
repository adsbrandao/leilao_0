<?php
use Models\Leilao;
?>
<div class="featured-car content-area">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1>Destaque</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
        </div>
        <!-- Slick slider area start -->
        <div class="slick-slider-area clearfix">
            <div class="row slick-carousel" data-slick='{"slidesToShow": 4, "responsive":[{"breakpoint": 1024,"settings":{"slidesToShow": 0}}, {"breakpoint": 768,"settings":{"slidesToShow": 0}}]}'>
                <div class="slick-slide-item">
                    <div class="car-box">
                    <?php
                        $leiloes = Leilao::findAll("status > 0");
                        while ($leilao = $leiloes->fetch()) {
                    ?>
                        <div class="car-thumbnail">
                            <a href="#" class="car-img">
                                <div class="tag">Em Andamento</div>
                                <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-1.jpg" alt="car">
                                <div class="facilities-list clearfix">
                                    <ul>
                                        <li>
                                            <span><i class="flaticon-calendar-1"></i></span>20/04/2020
                                        </li>
                                    </ul>
                                </div>
                            </a>
                            <div class="carbox-overlap-wrapper">
                                <div class="overlap-box">
                                    <div class="overlap-btns-area">
                                        <a class="overlap-btn" data-toggle="modal" data-target="#carOverviewModal">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                        <a href="#" class="overlap-btn">
                                            <i class="fa fa-balance-scale"></i>
                                        </a>
                                        <div class="car-magnify-gallery">
                                            <a href="img/car/car-1.jpg" class="overlap-btn" data-sub-html="<h4>Descricao</p>">
                                                <i class="fa fa-expand"></i>
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Ferrari Red Car</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Bmw e46 m3 Diski Serie</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Volkswagen Scirocco 2016</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-5.jpg" class="hidden" data-sub-html="<h4>Porsche Cayen Last</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="hidden-img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="detail">
                            <h1 class="title">
                                <a href="#"><?= $leilao->nome; ?> </a>
                            </h1>
                            <div class="location">
                                <a href="#">
                                Lote: <?= $leilao->lote; ?> | N°  <?= $leilao->status; ?> 
                                </a>
                            </div>
                        </div>
                        <div class="footer clearfix">
                            <div class="pull-right">
                                <p class="price">R$<?= $leilao->valor_inicial; ?></p>
                            </div>
                        </div>
                    
                    </div>
                    <?php
                    }
                    ?>
                </div>
                <div class="slick-slide-item">
                    <div class="car-box">
                        <div class="car-thumbnail">
                            <a href="#" class="car-img">
                                <div class="tag">Em Andamento</div>
                                <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-2.jpg" alt="car">
                                <div class="facilities-list clearfix">
                                    <ul>
                                        <li>
                                            <span><i class="flaticon-calendar-1"></i></span>20/04/2019
                                        </li>
                                    </ul>
                                </div>
                            </a>
                            <div class="carbox-overlap-wrapper">
                                <div class="overlap-box">
                                    <div class="overlap-btns-area">
                                        <a class="overlap-btn" data-toggle="modal" data-target="#carOverviewModal">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                        <a href="#" class="overlap-btn">
                                            <i class="fa fa-balance-scale"></i>
                                        </a>
                                        <div class="car-magnify-gallery">
                                            <a href="<?=\URL;?>site/img/car/car-2.jpg" class="overlap-btn" data-sub-html="<h4>Ferrari Red Car</h4>Descricao</p>">
                                                <i class="fa fa-expand"></i>
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="hidden-img">
                                            </a>
                                            <a href="<?=\URL;?>site/img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Ferrari Red Car</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="hidden-img">
                                            </a>
                                            <a href="<?=\URL;?>site/img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Bmw e46 m3 Diski Serie</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="hidden-img">
                                            </a>
                                            <a href="<?=\URL;?>site/img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Volkswagen Scirocco 2016</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="hidden-img">
                                            </a>
                                            <a href="<?=\URL;?>site/img/car/car-5.jpg" class="hidden" data-sub-html="<h4>Porsche Cayen Last</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="hidden-img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="detail">
                            <h1 class="title">
                                <a href="#">Ferrari Red Car</a>
                            </h1>
                            <div class="location">
                                <a href="#">
                                    Lote: 0123 | N° 05648544
                                </a>
                            </div>
                        </div>
                        <div class="footer clearfix">
                            <div class="pull-right">
                                <p class="price">R$850.00</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slick-slide-item">
                    <div class="car-box">
                        <div class="car-thumbnail">
                            <a href="#" class="car-img">
                                <div class="tag">Em Andamento</div>
                                <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-3.jpg" alt="car">
                                <div class="facilities-list clearfix">
                                    <ul>
                                        <li>
                                            <span><i class="flaticon-calendar-1"></i></span>20/04/2019
                                        </li>
                                    </ul>
                                </div>
                            </a>
                            <div class="carbox-overlap-wrapper">
                                <div class="overlap-box">
                                    <div class="overlap-btns-area">
                                        <a class="overlap-btn" data-toggle="modal" data-target="#carOverviewModal">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                        <a href="#" class="overlap-btn">
                                            <i class="fa fa-balance-scale"></i>
                                        </a>
                                        <div class="car-magnify-gallery">
                                            <a href="img/car/car-3.jpg" class="overlap-btn" data-sub-html="<h4>Bmw e46 m3 Diski Serie</h4>Descricao</p>">
                                                <i class="fa fa-expand"></i>
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Ferrari Red Car</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Volkswagen Scirocco 2016</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-5.jpg" class="hidden" data-sub-html="<h4>Porsche Cayen Last</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="hidden-img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="detail">
                            <h1 class="title">
                                <a href="#">Bmw e46 m3 Diski Serie</a>
                            </h1>
                            <div class="location">
                                <a href="#">
                                    Lote: 0123 | N° 05648544
                                </a>
                            </div>
                        </div>
                        <div class="footer clearfix">
                            <div class="pull-right">
                                <p class="price">R$850.00</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slick-slide-item">
                    <div class="car-box">
                        <div class="car-thumbnail">
                            <a href="#" class="car-img">
                                <div class="tag">Em Andamento</div>
                                <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-4.jpg" alt="car">
                                <div class="facilities-list clearfix">
                                    <ul>
                                        <li>
                                            <span><i class="flaticon-calendar-1"></i></span>20/04/2019
                                        </li>
                                    </ul>
                                </div>
                            </a>
                            <div class="carbox-overlap-wrapper">
                                <div class="overlap-box">
                                    <div class="overlap-btns-area">
                                        <a class="overlap-btn" data-toggle="modal" data-target="#carOverviewModal">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                        <a href="#" class="overlap-btn">
                                            <i class="fa fa-balance-scale"></i>
                                        </a>
                                        <div class="car-magnify-gallery">
                                            <a href="img/car/car-4.jpg" class="overlap-btn" data-sub-html="<h4>Volkswagen Scirocco 2016</h4>Descricao</p>">
                                                <i class="fa fa-expand"></i>
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Ferrari Red Car</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Bmw e46 m3 Diski Serie</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-5.jpg" class="hidden" data-sub-html="<h4>Porsche Cayen Last</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="hidden-img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="detail">
                            <h1 class="title">
                                <a href="#">Volkswagen Scirocco 2016</a>
                            </h1>
                            <div class="location">
                                <a href="#">
                                    Lote: 0123 | N° 05648544
                                </a>
                            </div>
                        </div>
                        <div class="footer clearfix">
                            <div class="pull-right">
                                <p class="price">R$850.00</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slick-slide-item">
                    <div class="car-box">
                        <div class="car-thumbnail">
                            <a href="#" class="car-img">
                                <div class="tag">Em Andamento</div>
                                <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-5.jpg" alt="car">
                                <div class="facilities-list clearfix">
                                    <ul>
                                        <li>
                                            <span><i class="flaticon-calendar-1"></i></span>20/04/2019
                                        </li>
                                    </ul>
                                </div>
                            </a>
                            <div class="carbox-overlap-wrapper">
                                <div class="overlap-box">
                                    <div class="overlap-btns-area">
                                        <a class="overlap-btn" data-toggle="modal" data-target="#carOverviewModal">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                        <a href="#" class="overlap-btn">
                                            <i class="fa fa-balance-scale"></i>
                                        </a>
                                        <div class="car-magnify-gallery">
                                            <a href="img/car/car-5.jpg" class="overlap-btn" data-sub-html="<h4>Porsche Cayen Last</h4>Descricao</p>">
                                                <i class="fa fa-expand"></i>
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Ferrari Red Car</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Bmw e46 m3 Diski Serie</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Volkswagen Scirocco 2016</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="hidden-img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="detail">
                            <h1 class="title">
                                <a href="#">Porsche Cayen Last</a>
                            </h1>
                            <div class="location">
                                <a href="#">
                                    Lote: 0123 | N° 05648544
                                </a>
                            </div>
                        </div>
                        <div class="footer clearfix">
                            <div class="pull-right">
                                <p class="price">R$850.00</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="slick-slide-item">
                    <div class="car-box">
                        <div class="car-thumbnail">
                            <a href="#" class="car-img">
                                <div class="tag">Em Andamento</div>
                                <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-6.jpg" alt="car">
                                <div class="facilities-list clearfix">
                                    <ul>
                                        <li>
                                            <span><i class="flaticon-calendar-1"></i></span>20/04/2019
                                        </li>
                                    </ul>
                                </div>
                            </a>
                            <div class="carbox-overlap-wrapper">
                                <div class="overlap-box">
                                    <div class="overlap-btns-area">
                                        <a class="overlap-btn" data-toggle="modal" data-target="#carOverviewModal">
                                            <i class="fa fa-eye-slash"></i>
                                        </a>
                                        <a href="#" class="overlap-btn">
                                            <i class="fa fa-balance-scale"></i>
                                        </a>
                                        <div class="car-magnify-gallery">
                                            <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Ferrari Red Car</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Bmw e46 m3 Diski Serie</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Volkswagen Scirocco 2016</h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descricao</p>">
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="hidden-img">
                                            </a>
                                            <a href="img/car/car-6.jpg" class="overlap-btn" data-sub-html="<h4>Lexus GS F</h4>Descricao</p>">
                                                <i class="fa fa-expand"></i>
                                                <img class="hidden" src="<?=\URL;?>site/img/car/car-6.jpg" alt="hidden-img">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="detail">
                            <h1 class="title">
                                <a href="#">Lexus GS F</a>
                            </h1>
                            <div class="location">
                                <a href="#">
                                    Lote: 0123 | N° 05648544
                                </a>
                            </div>
                        </div>
                        <div class="footer clearfix">
                            <div class="pull-right">
                                <p class="price">R$850.00</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slick-prev slick-arrow-buton">
                <i class="fa fa-angle-left"></i>
            </div>
            <div class="slick-next slick-arrow-buton">
                <i class="fa fa-angle-right"></i>
            </div>
        </div>
    </div>
</div>