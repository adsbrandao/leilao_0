<?php
use Models\Leilao;
?>
<div class="featured-car content-area">
    <div class="container">
        <div class="main-title">
            <h1>Leilão</h1>
            <p>Cenouras Lorem ipsum, descontos aumentados, mas fazem occaecat</p>
        </div>
        <div class="row">
        <?php
            $leiloes = Leilao::findAll("status > 0");
            while ($leilao = $leiloes->fetch()) {
            ?>
            <div class="col-lg-4 col-md-6">
            <a href="<?= URL ?>leilao/<?= $leilao->id; ?>">
                <div class="car-box-3">
                    <div class="car-thumbnail">
                        <a href="#" class="car-img">
                            <div class="for">Em Andamento</div>
                            <div class="price-box">
                                <span>R$<?= $leilao->valor_inicial; ?></span>
                            </div>
                            <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-1.jpg" alt="carro">
                        </a>
                        <div class="carbox-overlap-wrapper">
                            <div class="overlap-box">
                                <div class="overlap-btns-area">
                                    <a class="overlap-btn" data-toggle="modal">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                    <a class="overlap-btn">
                                        <i class="fa fa-balance-scale"></i>
                                    </a>
                                    <div class="car-magnify-gallery">
                                    <?php
                                        if (is_null($leilao->imagem_thumb) || !is_file(\PATH . 'upload/' . $leilao->imagem_thumb)) {
                                            echo '<img src="' . \URL . 'theme/img/logo.png" style="opacity:0.3;width: 100%;max-height: 200px;" />';
                                        } else {
                                            echo '<img src="' . \URL . 'upload/' . $leilao->imagem_thumb . '" style="width: 100%;max-height: 200px;" />';

                                        ?>
                                        <a href="img/car/car-1.jpg" class="overlap-btn" data-sub-html="<h4>Descrição</p>">
                                            <i class="fa fa-expand"></i>
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-5.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="oculto-img">
                                        </a>
                                        <?php 
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail">
                        <h1 class="title">
                            <a href="#">Nome <?= $leilao->nome; ?></a>
                        </h1>
                        <ul class="custom-list">
                            <li>
                                <a href="#">Lote: <?= $leilao->lote; ?> | N°  <?= $leilao->status; ?> </a>
                            </li>
                        </ul>
                        <ul class="facilities-list clearfix">
                           <p>lerem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem</p>
                        </ul>
                    </div>
                    <div class="footer clearfix">
                        
                    </div>
                </div>
                </a>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="car-box-3">
                    <div class="car-thumbnail">
                        <a href="#" class="car-img">
                            <div class="for">Em Andamento</div>
                            <div class="price-box">
                                <span>R$780.00</span>
                            </div>
                            <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-2.jpg" alt="carro">
                        </a>
                        <div class="carbox-overlap-wrapper">
                            <div class="overlap-box">
                                <div class="overlap-btns-area">
                                    <a class="overlap-btn" data-toggle="modal">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                    <a class="overlap-btn">
                                        <i class="fa fa-balance-scale"></i>
                                    </a>
                                    <div class="car-magnify-gallery">
                                        <a href="img/car/car-2.jpg" class="overlap-btn" data-sub-html="<h4>Descrição</p>">
                                            <i class="fa fa-expand"></i>
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-5.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="oculto-img">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail">
                        <h1 class="title">
                            <a href="#">Carro vermelho ferrari</a>
                        </h1>
                        <ul class="custom-list">
                            <li>
                                <a href="#">Lote: 0123 | N° 05648544</a>
                            </li>
                        </ul>
                        <ul class="facilities-list clearfix">
                           <p>lerem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem</p>
                        </ul>
                    </div>
                    <div class="footer clearfix">
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="car-box-3">
                    <div class="car-thumbnail">
                        <a href="#" class="car-img">
                            <div class="for">Em Andamento</div>
                            <div class="price-box">
                                <span>R$940.00</span>
                            </div>
                            <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-3.jpg" alt="carro">
                        </a>
                        <div class="carbox-overlap-wrapper cc_pointer">
                            <div class="overlap-box">
                                <div class="overlap-btns-area">
                                    <a class="overlap-btn" data-toggle="modal">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                    <a class="overlap-btn">
                                        <i class="fa fa-balance-scale"></i>
                                    </a>
                                    <div class="car-magnify-gallery">
                                        <a href="img/car/car-3.jpg" class="overlap-btn" data-sub-html="<h4>Descrição</p>">
                                            <i class="fa fa-expand"></i>
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-5.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="oculto-img">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail">
                        <h1 class="title">
                            <a href="#">Série Bmw e46 m3 Diski</a>
                        </h1>
                        <ul class="custom-list">
                            <li>
                                <a href="#">Lote: 0123 | N° 05648544</a>
                            </li>
                        </ul>
                        <ul class="facilities-list clearfix">
                           <p>lerem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem</p>
                        </ul>
                    </div>
                    <div class="footer clearfix">
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="car-box-3">
                    <div class="car-thumbnail">
                        <a href="#" class="car-img">
                            <div class="for">Em Andamento</div>
                            <div class="price-box">
                                <span>R$1050.00</span>
                            </div>
                            <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-4.jpg" alt="carro">
                        </a>
                        <div class="carbox-overlap-wrapper">
                            <div class="overlap-box">
                                <div class="overlap-btns-area">
                                    <a class="overlap-btn" data-toggle="modal">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                    <a class="overlap-btn">
                                        <i class="fa fa-balance-scale"></i>
                                    </a>
                                    <div class="car-magnify-gallery">
                                        <a href="img/car/car-4.jpg" class="overlap-btn" data-sub-html="<h4>Descrição</p>">
                                            <i class="fa fa-expand"></i>
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-5.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="oculto-img">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail">
                        <h1 class="title">
                            <a href="#">Volkswagen Scirocco 2016</a>
                        </h1>
                        <ul class="custom-list">
                            <li>
                                <a href="#">Lote: 0123 | N° 05648544</a>
                            </li>
                        </ul>
                        <ul class="facilities-list clearfix">
                           <p>lerem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem</p>
                        </ul>
                    </div>
                    <div class="footer clearfix">
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="car-box-3">
                    <div class="car-thumbnail">
                        <a href="#" class="car-img">
                            <div class="for">Em Andamento</div>
                            <div class="price-box">
                                <span>R$780.00</span>
                            </div>
                            <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-5.jpg" alt="carro">
                        </a>
                        <div class="carbox-overlap-wrapper">
                            <div class="overlap-box">
                                <div class="overlap-btns-area">
                                    <a class="overlap-btn" data-toggle="modal">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                    <a class="overlap-btn">
                                        <i class="fa fa-balance-scale"></i>
                                    </a>
                                    <div class="car-magnify-gallery">
                                        <a href="img/car/car-5.jpg" class="overlap-btn" data-sub-html="<h4>Descrição</p>">
                                            <i class="fa fa-expand"></i>
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-5.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="oculto-img">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail">
                        <h1 class="title">
                            <a href="#">Porsche Cayen Last</a>
                        </h1>
                        <ul class="custom-list">
                            <li>
                                <a href="#">N° ****</a>
                            </li>
                        </ul>
                        <ul class="facilities-list clearfix">
                           <p>lerem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem</p>
                        </ul>
                    </div>
                    <div class="footer clearfix">
                        
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="car-box-3">
                    <div class="car-thumbnail">
                        <a href="#" class="car-img">
                            <div class="for">Em Andamento</div>
                            <div class="price-box">
                                <span>R$940.00</span>
                            </div>
                            <img class="d-block w-100" src="<?=\URL;?>site/img/car/car-6.jpg" alt="carro">
                        </a>
                        <div class="carbox-overlap-wrapper">
                            <div class="overlap-box">
                                <div class="overlap-btns-area">
                                    <a class="overlap-btn" data-toggle="modal">
                                        <i class="fa fa-eye-slash"></i>
                                    </a>
                                    <a class="overlap-btn">
                                        <i class="fa fa-balance-scale"></i>
                                    </a>
                                    <div class="car-magnify-gallery">
                                        <a href="img/car/car-6.jpg" class="overlap-btn" data-sub-html="<h4>Lexus GS F</h4><p>A beautiful Sunrise this morning taken En-route to Keswick not one as planned but I'm extremely happy....</p>">
                                            <i class="fa fa-expand"></i>
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-6.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-2.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-2.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-3.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-3.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-4.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-4.jpg" alt="oculto-img">
                                        </a>
                                        <a href="img/car/car-1.jpg" class="hidden" data-sub-html="<h4>Descrição</p>">
                                            <img class="hidden" src="<?=\URL;?>site/img/car/car-1.jpg" alt="oculto-img">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="detail">
                        <h1 class="title">
                            <a href="#">Lexus GS F</a>
                        </h1>
                        <ul class="custom-list">
                            <li>
                                <a href="#">Lote: 0123 | N° 05648544</a>
                            </li>
                        </ul>
                        <ul class="facilities-list clearfix">
                           <p>lerem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem</p>
                        </ul>
                    </div>
                    <div class="footer clearfix">
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>