<?php 
use Models\Leilao;
use Models\Lance;
use Models\LeilaoFoto;
$leilao = Leilao::find($id);

// echo $leilao->processo;
?> 
<div class="sub-banner">
    <div class="container breadcrumb-area">
        <div class="breadcrumb-areas">
            <h1>Leilão Detalhe</h1>
            <ul class="breadcrumbs">
                <li><a href="#">Início</a></li>
                <li class="active">Leilão Detalhe</li>
            </ul>
        </div>
    </div>
</div>
<!-- Sub Banner end -->

<!-- Car details page start -->
<div class="car-details-page content-area-6">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-xs-12">
                <div class="car-details-section">
                    <!-- Heading start -->
                    <div class="heading-car clearfix">
                        <div class="pull-left">
                            <h3>Titulo Leilão</h3>
                            <h6>
                            Lote: <?= $leilao->lote; ?> | N°  <?= $leilao->status; ?> 
                            </h6>
                        </div>
                        <div class="pull-right">
                            <div class="price-box-3"><sup>R$</sup><?= $leilao->valor_inicial; ?><span></span></div>
                        </div>
                    </div>
                    <!-- carDetailsSlider start -->
                    <div id="carDetailsSlider" class="carousel car-details-sliders slide slide-2">
                        <!-- main slider carousel items -->
                        <div class="carousel-inner">
                            <div class="active item carousel-item" data-slide-number="0">
                                <img src="<?=\URL?>/site/img/car/car-5.jpg" class="img-fluid" alt="slider-car">
                            </div>
                            <div class="item carousel-item" data-slide-number="1">
                                <img src="<?=\URL?>/site/img/car/car-2.jpg" class="img-fluid" alt="slider-car">
                            </div>
                            <div class="item carousel-item" data-slide-number="2">
                                <img src="<?=\URL?>/site/img/car/car-3.jpg" class="img-fluid" alt="slider-car">
                            </div>
                            <div class="item carousel-item" data-slide-number="4">
                                <img src="<?=\URL?>/site/img/car/car-1.jpg" class="img-fluid" alt="slider-car">
                            </div>
                            <div class="item carousel-item" data-slide-number="5">
                                <img src="<?=\URL?>/site/img/car/car-4.jpg" class="img-fluid" alt="slider-car">
                            </div>
                        </div>
                        <!-- main slider carousel nav controls -->
                        <ul class="carousel-indicators car-properties list-inline nav nav-justified">
                            <li class="list-inline-item active">
                                <a id="carousel-selector-0" class="selected" data-slide-to="0" data-target="#carDetailsSlider">
                                    <img src="<?=\URL?>/site/img/car/car-5.jpg" class="img-fluid" alt="small-car">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a id="carousel-selector-1" data-slide-to="1" data-target="#carDetailsSlider">
                                    <img src="<?=\URL?>/site/img/car/car-2.jpg" class="img-fluid" alt="small-car">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a id="carousel-selector-2" data-slide-to="2" data-target="#carDetailsSlider">
                                    <img src="<?=\URL?>/site/img/car/car-3.jpg" class="img-fluid" alt="small-car">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a id="carousel-selector-3" data-slide-to="3" data-target="#carDetailsSlider">
                                    <img src="<?=\URL?>/site/img/car/car-1.jpg" class="img-fluid" alt="small-car">
                                </a>
                            </li>
                            <li class="list-inline-item">
                                <a id="carousel-selector-4" data-slide-to="4" data-target="#carDetailsSlider">
                                    <img src="<?=\URL?>/site/img/car/car-4.jpg" class="img-fluid" alt="small-car">
                                </a>
                            </li>
                        </ul>
                    </div>
                    <!-- Advanced search start -->
                    <div class="widget-2 advanced-search bg-grea-2 as-2">
                        <h3 class="sidebar-title">Em Andamento</h3>
                        <div class="s-border"></div>
                        <div class="m-border"></div>
                        <ul>
                            <li>
                                <span>Início Leilão:</span> 19/04/2021 13:14
                            </li>
                            <li>
                                <span>encerramento do Leilão:</span>19/04/2021 14:05
                            </li>
                            <li>
                                <span>Valor inícial:</span>R$ 1.000.00  
                            </li>
                            <li>
                                <span>Incremento:</span>R$ 10.000.00
                            </li>
                            <li>
                                <span>Parcelas</span> -
                            </li>
                            <li>
                                <span>Processo</span> -
                            </li>
                        </ul>
                    </div>
                    <!-- Tabbing box start -->
                    <div class="tabbing tabbing-box mb-40">
                        <ul class="nav nav-tabs" id="carTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="one" aria-selected="false">Descrição</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="two" aria-selected="false">Ver Edital</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="carTabContent">
                            <div class="tab-pane fade active show" id="one" role="tabpanel" aria-labelledby="one-tab">
                                <div class="car-description mb-50">
                                    <h3 class="heading-2">
                                        Descrição
                                    </h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat volutpat. Curabitur convallis fringilla diam sed aliquam. Sed tempor iaculis massa faucibus feugiat. In fermentum facilisis massa, a consequat purus viverra a. Aliquam pellentesque nibh et nibh feugiat gravida. Maecenas ultricies, diam vitae semper placerat, velit risus accumsan nisl, eget tempor lacus est vel nunc. Proin accumsan elit sed neque euismod fringilla. Curabitur lobortis nunc velit, et fermentum urna dapibus non. Vivamus magna lorem, elementum id gravida ac, laoreet tristique augue. Maecenas dictum lacus eu nunc porttitor, ut hendrerit arcu efficitur.</p>
                                    <p>Aliquam ultricies nunc porta metus interdum mollis. Donec porttitor libero augue, vehicula tincidunt lectus placerat a. Sed tincidunt dolor non sem dictum dignissim. Nulla vulputate orci felis, ac ornare purus ultricies a. Fusce euismod magna orci, sit amet aliquam turpis dignissim ac. In at tortor at ligula pharetra sollicitudin. Sed tincidunt, purus eget laoreet elementum, felis est pharetra ante, tincidunt feugiat libero enim sed risus. Sed at leo sit amet mi bibendum aliquam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent cursus varius odio, non faucibus dui. Nunc vehicula lectus sed velit suscipit aliquam vitae eu ipsum. adipiscing elit.</p>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="two" role="tabpanel" aria-labelledby="two-tab">
                                <div class="car-description mb-50">
                                    <div class="Características-info mb-50">
                                        <h3 class="heading-2">Ver Edital</h3>
                                        <p>Aliquam ultricies nunc porta metus interdum mollis. Donec porttitor libero augue, vehicula tincidunt lectus placerat a. Sed tincidunt dolor non sem dictum dignissim. Nulla vulputate orci felis, ac ornare purus ultricies a. Fusce euismod magna orci, sit amet aliquam turpis dignissim ac. In at tortor at ligula pharetra sollicitudin. Sed tincidunt, purus eget laoreet elementum, felis est pharetra ante, tincidunt feugiat libero enim sed risus. Sed at leo sit amet mi bibendum aliquam. Interdum et malesuada fames ac ante ipsum primis in faucibus. Praesent cursus varius odio, non faucibus dui. Nunc vehicula lectus sed velit suscipit aliquam vitae eu ipsum. adipiscing elit.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="contact-2 ca mb-50">
                        <h3 class="heading-2">Formulário de Contato</h3>
                        <form action="#" method="GET" enctype="multipart/form-data">
                            <div class="row">
                                <div class="form-group name col-md-12">
                                    <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome">
                                </div>
                                <div class="form-group email col-md-6">
                                    <input type="email" name="email" id="email" class="form-control" placeholder="E-mail">
                                </div>
                                <div class="form-group number col-md-6">
                                    <input type="text" name="telefone" id="telefone" class="form-control" placeholder="Telefone">
                                </div>
                                <div class="form-group message col-md-12">
                                    <textarea class="form-control" name="mensagem" id="mensagem" placeholder="Mensagem"></textarea>
                                </div>
                                <div class="send-btn col-md-12">
                                    <button type="submit" class="btn btn-md button-theme">Entrar em contato</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="sidebar-right">
                    <!-- Advanced search start -->
                    <div class="widget advanced-search d-none-992">
                        <h3 class="sidebar-title">Em Andamento</h3>
                        <div class="s-border"></div>
                        <div class="m-border"></div>
                        <ul>
                            <li>
                                <span>Início Leilão:</span> 19/04/2021 13:14
                            </li>
                            <li>
                                <span>Encerramento do Leilão:</span>19/04/2021 14:05
                            </li>
                            <li>
                                <span>Valor inícial:</span>R$ 1.000.00  
                            </li>
                            <li>
                                <span>Incremento:</span>R$ 10.000.00
                            </li>
                            <li>
                                <span>Parcelas</span> -
                            </li>
                            <li>
                                <span>Processo</span> -
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>