<div class="banner" id="banner">
    <div id="bannerCarousole" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner banner-slider-inner text-center">
            <div class="carousel-item banner-max-height active item-bg">
                <img class="d-block w-100 h-100" src="<?=\URL;?>site/img/banner/img-4.jpg" alt="banner">
                <div class="carousel-content container banner-info-2 bi-2 text-l">
                    <h2>Bem vindo a norte de minas</h2>
                    <p>Aqui você encontra uma variedades de leilões!</p>
                    <a href="#" class="btn btn-lg btn-round btn-theme">Comece agora</a>
                    <a href="#" class="btn btn-round btn-white-lg-outline">Saiba mais</a>
                </div>
            </div>
            <div class="carousel-item banner-max-height item-bg">
                <img class="d-block w-100 h-100" src="<?=\URL;?>site/img/banner/img-3.jpg" alt="banner">
                <div class="carousel-content container banner-info-2 bi-2 text-l">
                    <h2>Titulo</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                    <a href="#" class="btn btn-lg btn-round btn-theme">Comece agora</a>
                    <a href="#" class="btn btn-round btn-white-lg-outline">Saiba mais</a>
                </div>
            </div>
            <div class="carousel-item banner-max-height item-bg">
                <img class="d-block w-100 h-100" src="<?=\URL;?>site/img/banner/img-2.jpg" alt="banner">
                <div class="carousel-content container banner-info-2 bi-2 text-l">
                    <h2>Titlo!</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</p>
                    <a href="#" class="btn btn-lg btn-round btn-theme">Comece agora</a>
                    <a href="#" class="btn btn-round btn-white-lg-outline">Saiba mais</a>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev none-580" href="#bannerCarousole" role="button" data-slide="prev">
            <span class="slider-mover-left" aria-hidden="true">
                <i class="fa fa-angle-left"></i>
            </span>
        </a>
        <a class="carousel-control-next none-580" href="#bannerCarousole" role="button" data-slide="next">
            <span class="slider-mover-right" aria-hidden="true">
                <i class="fa fa-angle-right"></i>
            </span>
        </a>
    </div>
</div>