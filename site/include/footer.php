<footer class="footer overview-bgi">
    <div class="container footer-inner">
        <div class="row">
            <div class="col-xl-4 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <img src="<?=\URL;?>site/img/logo_branca.png" alt="logo" class="f-logo">
                    <div class="s-border"></div>
                    <div class="m-border"></div>
                    <div class="text">
                        <P>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque. Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien vitae lectus suscipit, et pulvinar nisi tincidunt. Aliquam erat.</P>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>
                        Contato
                    </h4>
                    <div class="s-border"></div>
                    <div class="m-border"></div>
                    <ul class="contact-info">
                        <li>
                            <i class="flaticon-pin"></i>Rua Peru, Residencial Recanto do Sol,  Cuiabá - MT
                        </li>
                        <li>
                            <i class="flaticon-mail"></i><a href="mailto:contato@nortedeminas.com">contato@nortedeminas.com</a>
                        </li>
                        <li>
                            <i class="flaticon-phone"></i><a href="tel:+5531999999999">(31) 99999-9999</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                <div class="footer-item">
                    <h4>
                        Links
                    </h4>
                    <div class="s-border"></div>
                    <div class="m-border"></div>
                    <ul class="links">
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Início</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Leilões</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Sobre-nós</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-angle-right"></i>Contato</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                <div class="footer-item clearfix">
                    <h4>Newsletter</h4>
                    <div class="s-border"></div>
                    <div class="m-border"></div>
                    <div class="Subscribe-box">
                        <p>Receba em as últimas notícias em seu e-mail!</p>
                        <form class="form-inline" action="#" method="GET">
                            <input type="text" class="form-control mb-sm-0" id="inlineFormInputName3" placeholder="E-mail">
                            <button type="submit" class="btn"><i class="fa fa-paper-plane"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <p class="copy">DESENVOLVIDO POR: <a target="in_blank" href="https://www.webajato.com.br/"><img style="width: 16%;" src="<?=\URL;?>site/img/webajato_logotipo_transparente.png" alt=""></a></p>
                </div>
                <div class="col-lg-6">
                    <div class="social-list-2">
                        <ul>
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>