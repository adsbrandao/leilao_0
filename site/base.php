<?php 
$content = ob_get_contents();
ob_clean();?>
<!DOCTYPE html>
<html lang="zxx">
<head>
    <title>Norte de Minas | Leilões</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <link rel="icon" href="<?=\URL;?>site/img/logo-icon.png" type="image/x-icon">

    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/css/animate.min.css">
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/css/bootstrap-submenu.css">

    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/css/bootstrap-select.min.css">
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/fonts/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/fonts/linearicons/style.css">
    <link rel="stylesheet" type="text/css"  href="<?=\URL;?>site/css/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" type="text/css"  href="<?=\URL;?>site/css/dropzone.css">
    <link rel="stylesheet" type="text/css"  href="<?=\URL;?>site/css/slick.css">
    <link rel="stylesheet" type="text/css"  href="<?=\URL;?>site/css/lightbox.min.css">
    <link rel="stylesheet" type="text/css"  href="<?=\URL;?>site/css/jnoty.css">

    <!-- Custom stylesheet -->
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/css/style.css">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="<?=\URL;?>site/css/skins/yellow.css">

    <!-- Favicon icon -->

    <!-- Google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,600,700%7CUbuntu:300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600,700,800,900&amp;display=swap" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="<?=\URL;?>site/css/ie10-viewport-bug-workaround.css">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script  src="<?=\URL;?>site/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script  src="<?=\URL;?>site/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script  src="<?=\URL;?>site/js/html5shiv.min.js"></script>
    <script  src="<?=\URL;?>site/js/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="page_loader"></div>

<div id="dv_main" style="opacity: 0;">
    <?php
    include(\PATH.'site/include/header.php');    
    echo $content??'';
    include(\PATH.'site/include/footer.php');
    ?>
</div>

<script src="<?=\URL;?>site/js/jquery-2.2.0.min.js"></script>
<script src="<?=\URL;?>site/js/popper.min.js"></script>
<script src="<?=\URL;?>site/js/bootstrap.min.js"></script>
<script  src="<?=\URL;?>site/js/bootstrap-submenu.js"></script>
<script  src="<?=\URL;?>site/js/rangeslider.js"></script>
<script  src="<?=\URL;?>site/js/jquery.mb.YTPlayer.js"></script>
<script  src="<?=\URL;?>site/js/bootstrap-select.min.js"></script>
<script  src="<?=\URL;?>site/js/jquery.easing.1.3.js"></script>
<script  src="<?=\URL;?>site/js/jquery.scrollUp.js"></script>
<script  src="<?=\URL;?>site/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script  src="<?=\URL;?>site/js/dropzone.js"></script>
<script  src="<?=\URL;?>site/js/slick.min.js"></script>
<script  src="<?=\URL;?>site/js/jquery.filterizr.js"></script>
<script  src="<?=\URL;?>site/js/jquery.magnific-popup.min.js"></script>
<script  src="<?=\URL;?>site/js/jquery.countdown.js"></script>
<script  src="<?=\URL;?>site/js/jquery.mousewheel.min.js"></script>
<script  src="<?=\URL;?>site/js/lightgallery-all.js"></script>
<script  src="<?=\URL;?>site/js/jnoty.js"></script>
<script  src="<?=\URL;?>site/js/app.js"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script  src="<?=\URL;?>site/js/ie10-viewport-bug-workaround.js"></script>
<!-- Custom javascript -->
<script  src="<?=\URL;?>site/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
