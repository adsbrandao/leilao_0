<?php namespace Core;

class ResponseSimple implements ResponseSimpleInterface {
    private $_body;
    private $_type = 'html';
    private $_httpCode = 200;
    private $_headers;
    /**
     *
     * @param String|Array $body Payload da resposta
     * @param Int $httpCode Numero correspondente ao code HTTP da resposta.
     * @param array $headers Array com headers adicionais
     */
    public function __construct ($body, $httpCode = 200, $headers = [])
    {
        $this->_headers = $headers;
        $this->_httpCode = $httpCode;
        if (is_array($body)) {
            $content = ob_get_contents();
            ob_clean();
            $this->_type = 'json';
            $this->_headers[] = 'Content-type: text/json;Charset=utf-8';
            $this->_body = json_encode($body, JSON_UNESCAPED_UNICODE | JSON_NUMERIC_CHECK);
        } else {
            $this->_type = 'html';
            $this->_headers[] = 'Content-type: text/html;Charset=utf-8';
            $this->_body = $body;
        }
    }

    public static function redirect ($url)
    {
        header('location:'.$url);
        exit;
    }

    /**
     * Add header
     *
     * @param String $name Exemplo Content-type
     * @param String $value Exemplo text/html
     * @return void
     */
    public function header ($name, $value)
    {
        $this->_headers[] = $name.":".$value;
    }


    /**
     * Envia a resposta
     *
     * @return void
     */
    public function send ()
    {
        ob_clean();
        foreach ($this->_headers as $header) {
            header($header);
        }
        http_response_code($this->_httpCode);

        echo $this->_body;
        exit;
    }
}