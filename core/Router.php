<?php namespace Core;

class Router {
    static $prefix = "";
    static $data;
    static $name;
    static $requestURL;
    
    public static function getMethod()
    {
        $method = ($_POST['_method'] ?? ($_GET['_method'] ?? $_SERVER['REQUEST_METHOD']));
        $method = strtoupper($method);

        return $method;
    }

    public static function checkMethod($method)
    {
        return static::getMethod()==strtoupper($method);
    }

    public static function checkMatchUrl ($url, $requestUrl)
    {

        // formato: /nome/:campo1/:campo2?
        $pattern = preg_replace(
            '/([\/]{0,1}[:][^\/\?]+)([\?]{0,1})/',
            '([/][^/]+)$2',
            $requestUrl
        );

        $pattern = str_replace('//', '/', $pattern);
        $pattern = str_replace('/', '\\/', $pattern);

        $uri = explode('?', $url);
        $request = array_shift($uri);
        $request = str_replace('//', '/', $request);
        if (!preg_match_all('/^'.$pattern.'$/', $request, $mroute, PREG_SET_ORDER, 0)) {
            
            return false;
        }

        return true;
    }

    public static function checkUrl ($route, $controller, &$params = [])
    {
        // formato: /nome/:campo1/:campo2?
        $pattern = preg_replace(
            '/([\/]{0,1}[:][^\/\?]+)([\?]{0,1})/',
            '([/][^/]+)$2',
            static::$prefix.$route,
            -1
        );

        $pattern = str_replace('//', '/', $pattern);
        $pattern = str_replace('/', '\\/', $pattern);

        $uri = explode('?', $_SERVER['REQUEST_URI']);
        $request = array_shift($uri);
        $request = str_replace('//', '/', $request);
        
        if (!preg_match_all('/^'.$pattern.'$/', $request, $mroute, PREG_SET_ORDER, 0)) {
            return false;
        }

        preg_match_all('/[:]([^\/\/?]+)/', $route, $mparams, PREG_SET_ORDER, 0);

        $params = [];
        foreach ($mparams as $i=>$m) {
            $j = $i+1;
            $v = (isset($mroute[0][$j]) && $mroute[0][$j] != '') ? $mroute[0][$j] : null;

            if (!is_null($v)) {
                $v = preg_replace('/^\//', '', $v);
            }

            // $params[$i] = $v;
            $params[$m[1]] = $v;
        }

        static::$data = [
            'method' => static::getMethod(),
            'route' => $request,
            'controller' => $controller,
            'request_uri' => $uri,
            'params' => $params
        ];
        return true;

    }

    public static function prefix($prefix, $routers)
    {
        $prefixBuffer = static::$prefix;
        static::$prefix .= $prefix;
        $routers();
        static::$prefix = $prefixBuffer;
    }
    public static function resource($prefix, $controller,$options = [])
    {
        $options['only'] = $options['only']?? [
            'index',//GET /prefix
            'create',//GET /prefix/create
            'store',//POST /prefix
            'show',//GET /prefix/{id}
            'edit',//GET /prefix/{id}/edit
            'update',//POST /prefix/{id}
            'delete'//DELETE /prefix/{id}
        ];
        $options['except'] = $options['except']??[];

        if($prefix!='' && substr($prefix,0,1)!='/')
            $prefix = "/".$prefix;

        $requests = [
            'index' => [
                'method' => 'GET',
                'request' => $prefix
            ],
            'create' => [
                'method' => 'GET',
                'request' => $prefix."/create"
            ],
            'store' => [
                'method' => 'POST',
                'request' => $prefix
            ],
            'show' => [
                'method' => 'GET',
                'request' => $prefix."/:id"
            ],
            'edit' => [
                'method' => 'GET',
                'request' => $prefix."/:id/edit"
            ],
            'update' => [
                'method' => 'PUT',
                'request' => $prefix."/:id"
            ],
            'update' => [
                'method' => 'POST',
                'request' => $prefix."/:id"
            ],
            'delete' => [
                'method' => 'DELETE',
                'request' => $prefix."/:id"
            ]
        ];
        
        foreach($requests as $i=>$data){
            if(in_array($i,$options['only']) && !in_array($i,$options['except'])){
                if(static::checkMethod($data['method'])){
                    $r = static::any($data['request'], $data['request'], $controller."@{$i}");
                    if($r!=false)
                        return true;
                }
            }
        }
    }

    public static function getSite ($nameRoute, $request, $filenameSite)
    {
        return self::get($nameRoute, $request, function ($arg = []) use ($filenameSite) {
            $files = [
                \PATH."site/".$filenameSite,
                \PATH."site/".$filenameSite.".php",
                \PATH."site/".$filenameSite.".html"
            ];

            foreach ($files as $file) {
                if (is_file($file)) {
                    include($file);
                    include(\PATH."site/base.php");
                    exit;
                }
            }
        });
    }

    public static function postSite ($nameRoute, $request, $filenameSite)
    {
        return self::post($nameRoute, $request, function ($args = []) use ($filenameSite) {
            $files = [
                \PATH."site/".$filenameSite,
                \PATH."site/".$filenameSite.".php",
                \PATH."site/".$filenameSite.".html"
            ];

            foreach ($files as $file) {
                if (is_file($file)) {
                    include($file);
                    include(\PATH."site/base.php");
                    exit;
                }
            }
        });
    }

    public static function get($nameRoute, $request,$controller)
    {
       
       if(static::checkMethod("GET"))
           return static::any($nameRoute, $request, $controller);
       
       return false;
    }

    public static function post($nameRoute, $request,$controller)
    {
        if(static::checkMethod("POST"))
            return static::any($nameRoute, $request, $controller);
            
            return false;
    }

    public static function put($nameRoute, $request,$controller)
    {
        if(static::checkMethod("PUT"))
            return static::any($nameRoute, $request, $controller);
            
            return false;
    }

    public static function delete($nameRoute, $request,$controller)
    {
        if(static::checkMethod("DELETE"))
            return static::any($nameRoute, $request, $controller);
            
            return false;
    }

    public static function any($nameRoute, $request, $controller){
        if(!static::checkUrl($request, $controller, $params)){
            return false;
        }
        
        static::$name = $nameRoute;
        static::$requestURL = $request;
        static::unknown($nameRoute, $controller, $params);
    }

    public static function response ($body)
    {
        if ($body === false) {
            return false;
        }

        if ($body instanceof ResponseSimpleInterface) {
            $body->send();
        } elseif(is_string($body) || is_array($body)) {
            $response = new ResponseSimple($body);
            $response->send();
        } else {
            throw new \Exception("Tipo de saida não permitida!");
        }

    }

    public static function unknown ($nameRoute, $controller,$args = [])
    {
        if(is_null(static::$data)) {
            list($uri) = explode("?", $_SERVER['REQUEST_URI']);
            static::$data = [
                'method' => static::getMethod(),
                'route' => null,
                'controller' => $controller,
                'request_uri' => strtolower( $uri )
            ];
        }

        if (is_callable($controller)) {
            return static::response($controller($args));
        }
        
        list($class, $method) = explode("@", $controller);
        
        if (substr($class, -10) != 'Controller') {
            $class .='Controller';
        }

        if (is_null($method) || empty($method)) {
            $method = 'index';
        }
        
        $classFile = \PATH . "app/Controllers/" . $class . ".php";
        $class = "App/Controllers/" . $class;
        
        $classFile = str_replace("/", DIRECTORY_SEPARATOR, $classFile);
        $class = str_replace("/", "\\", $class);
        
        if(!is_file($classFile)) {
            throw new \Exception("Arquivo não existe '{$classFile}'");
        }
            
        require_once $classFile;
        $response = call_user_func_array(array(new $class, $method), $args);

        return static::response($response);
    }
}