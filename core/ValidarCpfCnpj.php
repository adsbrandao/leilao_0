<?php namespace Core;

class ValidarCpfCnpj {
    public static function testCPF ($valor)
    {
        $valor = preg_replace('/[^0-9]/', '', $valor);

        if (strlen($valor) != 11) return false;

        for ($i = 0; $i<10;$i++) {
            if ($valor == str_repeat(''.$i, 11)) {
                return false;
            }
        }

        $valor = str_split($valor);

        $d1 = 0;
        $d2 = 0;

        for($i = 0;$i < 9;$i++) {
            $d1 += $valor[$i] * (10 - $i);
        }

        $d1 = $d1 % 11;

        if ($d1 == 0 || $d1 == 1) {
            $d1 = 0;
        } else {
            $d1 = 11 - $d1;
        }

        for($i = 1;$i < 10;$i++) {
            $d2 += $valor[$i] * (10 - $i + 1);
        }

        $d2 = $d2 % 11;

        if ($d2 == 0 || $d2 == 1) {
            $d2 = 0;
        } else {
            $d2 = 11 - $d2;
        }

        return ($d1 == $valor[9] && $d2 == $valor[10]);

    }

    public static function testCNPJ ($valor)
    {
        $valor = preg_replace('/[^0-9]/', '', $valor);
        
        if (strlen($valor) != 14) return false;

        for ($i = 0; $i<10;$i++) {
            if ($valor == str_repeat(''.$i, 14)) {
                return false;
            }
        }

        $valor = str_split($valor);

        $d1 = 0;
        $pesos = [5,4,3,2,9,8,7,6,5,4,3,2];
        $i = 0;
        foreach($pesos as $peso) {
            $d1 += $valor[$i] * $peso;
            $i++;
        }
        
        $d1 = $d1 % 11;
        if ($d1 < 2) {
            $d1 = 0;
        } else {
            $d1 = 11 - $d1;
        }

        $d2 = 0;
        $pesos = [6,5,4,3,2,9,8,7,6,5,4,3,2];
        $i = 0;
        foreach($pesos as $peso) {
            $d2 += $valor[$i] * $peso;
            $i++;
        }

        $d2 = $d2 % 11;
        if ($d2 < 2) {
            $d2 = 0;
        } else {
            $d2 = 11 - $d2;
        }

        return ($d1 == $valor[12] && $d2 == $valor[13]);

    }
}