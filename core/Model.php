<?php namespace Core;

class Model implements ModelInterface {
    private $_table;
    private $_primaryKey;
    private $_connectionsId;
    private $_fields;
    private $_select = '*';
    /**
     * @var Record
     */
    private $_record;

    public function __construct ()
    {
        $this->_record = new Record([]);
        $this->_record->setModel(get_class($this));
    }

    public function __set ($name, $value)
    {
        if(is_null($name) || empty($name)) return false;
        if (is_null($this->_record)) {
            $this->_record = new Record();
        }
        $this->_record->{$name} = $value;
    }

    public function __get ($name)
    {
        if (is_null($this->_record)) {
            $this->getFields();
        }
        return $this->_record->{$name};
    }
    /**
     * Define a tabela do model
     *
     * @param String $tableName
     * @return void
     */
    public function setConnectionId($connectionsId)
    {
        $this->_connectionsId = $connectionsId;
    }
    /**
     * Define a tabela do model
     *
     * @param String $tableName
     * @return void
     */
    public function setTable($tableName)
    {
        $this->_table = $tableName;
    }
    /**
     * Define o campo chave primary
     *
     * @param String $fieldName
     * @return void
     */
    public function setPrimaryKey($fieldName)
    {
        $this->_primaryKey = $fieldName;
    }

    public function getTable()
    {
        return $this->_table;
    }
    public function getPrimaryKey()
    {
        return $this->_primaryKey;
    }

    public function setFieldDefault ($fieldName, $default = null, $permitNull = true)
    {
        $primaryKey = $this->getPrimaryKey();
        $this->getFields();
        $this->_fields[$fieldName]['default'] = $default;
        $this->_fields[$fieldName]['permitNull'] = $permitNull;

        if (isset($this->_record->{$primaryKey}) && is_null($this->_record->{$primaryKey})) {
            $this->_record->{$field['name']} = $default;
        }
    }

    public function setFields ($fields)
    {
        $fieldsNovo = [];

        foreach ($fields as $k => $v) {
            $field = [];
            if (is_string($k)) {
                $name = $k;
                $field['name'] = $name;
                if(is_string($v)) {
                    $field['default'] = $v;
                } else {
                    $field = $v;
                }
            } elseif(is_string($v)) {
                $name = $v;
                $field['name'] = $name;
            } else {
                $field = $v;
            }

            $name = $field['name']??$name;

            $field['table']??null;
            $field['name'] = $field['name']??$name;
            $field['native_type'] = $field['native_type']??null;
            $field['pdo_type'] = $field['pdo_type']??null;
            $field['type'] = 'String';
            $field['len'] = $field['len']??null;
            $field['precision'] = $field['precision']??null;
            $field['default'] = $field['default']??null;
            $field['permitNull'] = $field['permitNull']??true;
            $this->_record->set($name, $field['default']);

            $fieldsNovo[] = $field;
        }

        $this->_fields = $fieldsNovo;
    }
    /**
     * Lista em array dos campos do model
     *
     * @return Array
     */
    public function getFields()
    {
        if (!is_null($this->_fields)) return $this->_fields;
        
        $result = Query::query("SELECT * FROM `{$this->_table}` WHERE 0 LIMIT 1", null, $this->_connectionsId);
        $c = $result->columnCount();
        $fields = [];

        for($i=0;$i<$c;$i++){
            $field = $result->getColumnMeta($i);
            $name = $field['name'];
            
            if (is_null($field['name'])) {
                continue;
            }

            $field['table']??null;
            $field['name'] = $field['name'];
            $field['native_type'] = $field['native_type']??null;
            $field['pdo_type'] = $field['pdo_type']??null;
            $field['type'] = 'String';
            $field['len'] = $field['len']??null;
            $field['precision'] = $field['precision']??null;
            $field['default'] = $field['default']??null;
            $field['permitNull'] = $field['permitNull']??true;

            if (!$this->_record->isset($name)) {
                $this->_record->set($name, $field['default']);
            }

            switch (strtolower($field['native_type'])){
                case 'integer':// -9999
                    $field['type'] = 'int';
                    break;
                case 'number':// -9999.99
                case 'float':
                case 'double':
                    $field['type'] = 'float';
                    break;
                default:
                    $field['type'] = 'String';
                    break;
            }

            $fields[$field['name']] = $field;
        }

        $this->_fields = $fields;

        return $fields;
    }
    public function select ($fields = '*')
    {
        $this->_select = $fields;
    }

    public function getSelect ()
    {
        return $this->_select;
    }
    /**
     * Executa INSERT INTO no banco
     *
     * @param Array $data Array com os valores ou NULL pagar direto dos paramentos anteriormente setados
     * @return this 
     */
    public function insert ($data = null)
    {
        if (is_null($data)) {
            $data = [];
        }

        $fields = $this->getFields();
        
        $fieldsName = [];
        $fieldsValues = [];
        foreach ($fields as $field) {
            if ($field['name'] == $this->getPrimaryKey()) {
                continue;
            }

            if (!isset($data[$field['name']]) && $this->_record->isset($field['name'])) {
                $data[$field['name']] = $this->_record->get($field['name']);
            }

            $value = $data[$field['name']] ?? null;

            if (is_null($value)) {
                continue;
            }
            $data[$field['name']] = $value;
            $this->_record->set($field['name'], $value);

            $fieldsName[] = $field['name'];
            $fieldsValues[] = '?';
            $bindData[] = $value;
        }

        try {
            if (empty($fieldsName)) {
                throw new \Exception("Sem Dados");
            }
            $sql = "INSERT INTO ".$this->getTable().
                " (".implode(",", $fieldsName).") ".
                "VALUES ".
                "(".implode(",", $fieldsValues).")";
            Query::query($sql, $bindData, $this->_connectionsId);
            $id = Query::lastInsertId($this->_connectionsId);
            $primaryKey = $this->getPrimaryKey();
            $this->_record->set($primaryKey, $id);
        } catch (\Exception $e) {
            throw new \Exception("Erro ao salvar " . $e->getMessage(), 1, $e);
        }

        return $id;
    }
    /**
     * Executa UPDATE no banco
     *
     * @param Int $id
     * @param Array $data Array com os valores ou NULL pagar direto dos paramentos anteriormente setados
     * @return this
     */
    public function update ($id, $data = null)
    {
        $fields = $this->getFields();
        $primaryKey = $this->getPrimaryKey();
        $bindData = [];        
        $fieldsValues = [];

        foreach ($fields as $field) {
            if ($field['name'] == $primaryKey) {
                continue;
            }

            $dataExiste = array_key_exists($field['name'], $data);
            if ($dataExiste) {
                $value = $data[$field['name']];
            } elseif ($this->_record->isset($field['name'])) {
                $value = $this->_record->get($field['name']);
            } else {
                continue;
            }

            $data[$field['name']] = $value;
            $this->_record->set($field['name'], $value);

            $fieldsValues[] = $field['name'] . ' = ?';
            $bindData[] = $value;
        }

        $where = $primaryKey . ' = '. $id;
        $this->_record->set($primaryKey, $id);

        try {
            $sql = "UPDATE ".$this->getTable()." SET ".implode(",", $fieldsValues)." WHERE ". $where;
            Query::query($sql, $bindData, $this->_connectionsId);

            return $this;
        } catch (\Exception $e) {
            throw new \Exception("Erro ao salvar", 1, $e);
        }
        return true;
    }
    public function delete ($where, $order = null, $limit = 1)
    {
        $bindData = [];
        Query::transformaWhere($this->getPrimaryKey(), $where, $bindData);

        if (is_null($order)) {
            $order = '';
        } else {
            $order = ' ORDER BY ' . $order;
        }

        try {
            $sql = "DELETE FROM ".$this->getTable()." WHERE ". $where . $order . " LIMIT ".$limit;
            Query::query($sql, $bindData, $this->_connectionsId);
        } catch (\Exception $e) {
            throw new \Exception("Erro ao salvar", 1, $e);
        }
        return true;
    }

    /**
     * Busca registro pelo primaryKey
     *
     * @param Int|String|Array $where Valor do primaryKey, Array para outros parametros para o WHERE
     * @param Array $bindData BindData
     * @param String $order
     * @return self|Boolean False se não encontrar
     */
    public static function find ($where, $bindData = [], $order = null)
    {
        $model = new static();
        Query::transformaWhere($model->getPrimaryKey(), $where, $bindData);

        if (is_null($order)) {
            $order = '';
        } else {
            $order = ' ORDER BY ' . $order;
        }

        try {
            $sql = "SELECT ".$model->getSelect()." FROM ".$model->getTable()." WHERE ". $where . $order . " LIMIT 1";
            $res = Query::query($sql, $bindData, $model->_connectionsId);
            if ($res->rowCount() == 0) {
                return false;
            }
            $model->setData($res->fetch(\PDO::FETCH_ASSOC));
            return $model;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 1, $e);
        }
    }

    /**
     * Busca uma lista de resultados
     *
     * @param Int|String $where
     * @param Array $bindData BindData
     * @param String $order
     * @param String $limit
     * @return Boolean|\PDOStatement False se erro
     */
    public static function findAll ($where = null, $bindData = [], $order = null, $limit = null)
    {
        $model = new static();
        Query::transformaWhere($model->getPrimaryKey(), $where, $bindData);

        if (is_null($order)) {
            $order = '';
        } else {
            $order = ' ORDER BY ' . $order;
        }
        

        if (is_null($limit)) {
            $limit = '';
        } else {
            $limit = ' LIMIT ' . $limit;
        }

        $sql = "SELECT ".$model->getSelect()." FROM ".$model->getTable()." WHERE ". $where . $order . $limit;
        if (!$res = Query::query($sql, $bindData, $model->_connectionsId)) {
            return false;
        }
        $res->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, '\\' . get_class($model));
        return $res;
    }

    /**
     * Executa query sql
     *
     * @param String $sql QuerySql
     * @param array $bindData
     * @return Boolean|\PDOStatement False se erro
     */
    public static function query ($sql, $bindData = [])
    {
        $model = new static();
        if (!$res = Query::query($sql, $bindData, $model->_connectionsId)) {
            return false;
        }
        return $res;

    }


    /**
     * Executa query de select sql (fetchmode: class)
     *
     * @param String $sql QuerySql
     * @param array $bindData
     * @return Boolean|\PDOStatement False se erro
     */
    public static function querySelect ($sql, $bindData = [])
    {
        $model = new static();
        if (!$res = Query::query($sql, $bindData, $model->_connectionsId)) {
            return false;
        }
        $res->setFetchMode(\PDO::FETCH_CLASS|\PDO::FETCH_PROPS_LATE, '\\' . get_class($model));
        return $res;
    } 

    static public function sizeof ($where = null, $bindData = [], $joins = null)
    {
        $model = new static();
        Query::transformaWhere($model->getPrimaryKey(), $where, $bindData);

        $sql = "SELECT count(".$model->getPrimaryKey().") FROM ";
        $sql .= $model->getTable();
        $sql .= is_null($joins)? '' : $joins;
        $sql .= " WHERE ".$where;

        if (!$res = Query::query($sql, $bindData, $model->_connectionsId)) {
            return false;
        }

        list($count) = $res->fetch(\PDO::FETCH_NUM);
        return $count;
    }

    public function dateFormat ($fieldName, $format = 'd/m/Y')
    {
        return $this->_record->dateFormat($fieldName, $format);
    }

    public function dateTimeFormat ($fieldName, $format = 'd/m/Y H:i:s')
    {
        return $this->_record->dateFormat($fieldName, $format);
    }

    /**
     *
     * @param Array $data
     * @return void Record
     */
    public function setData ($data)
    {
        $this->_record = new Record($data);
    }

    /**
     * Retorna dados como array
     *
     * @return Array
     */
    public function toArray()
    {
        $this->getFields();
        return $this->_record->toArray();
    }
}