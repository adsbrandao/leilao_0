<?php namespace Core;

class UserData {
    const CLIENTE = 1;
    const USUARIO = 2;
    const STATUS_INATIVO = 0;
    const STATUS_ATIVO = 1;
    const STATUS_NOVO = 2;
    public $usuario_id;
    public $cliente_id;
    public $grupo_id; // Caso usuario: o grupo.Caso cliente: null
    public $cliente_status = -1; // -1 - nao logado / 0 - Inativo / 1 - Ativo / 2 - Novo
    public $usuario_status = -1; // -1 - nao logado / 0 - Inativo / 1 - Ativo / 2 - Novo
    public $imagem;
    public $nome;
    public $telefone;
    public $celular;
    public $email;
    public $data_create;
    public $data_update;
    public $data_acesso;
}