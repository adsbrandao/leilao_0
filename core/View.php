<?php namespace Core;

class View {
    static $_title = '';
    static $_siteName = '';
    static $_breadcrumb = [];
    static $_titleAction = '';
    static function getSiteName ()
    {
        return View::$_siteName;
    }
    static function getTitle ()
    {
        return View::$_title;
    }
    static function getTitleAction ()
    {
        return View::$_titleAction;
    }
    static function getBreadcrumb ()
    {
        return View::$_breadcrumb;
    }
    static function addBreadcrumb ($title, $href)
    {
        View::$_breadcrumb[] = (object)['title' => $title, 'href' => $href];
    }
    static function setSiteName ($value)
    {
        View::$_siteName = $value;
    }
    static function setTitle ($value)
    {
        View::$_title = $value;
    }

    static function setTitleAction ($html)
    {
        View::$_titleAction = $html;
    }

    static function appendTitleAction ($html)
    {
        View::$_titleAction .= ' '.$html;
    }

    static function useComponent ($name)
    {
        $file = PATH."theme/componentes/{$name}.php";

        if (is_file($file)) {
            include_once($file);
            return true;
        }
        return false;
    }

    static function getTemplate ($template = 'default')
    {
        $path = \PATH . "app/views/templates/";
        $filename = str_replace("/", DIRECTORY_SEPARATOR, "{$path}/{$template}");

        $files = [
            $filename,
            $filename.".php",
            $filename.".html"
        ];
        
        $content = ob_get_contents();
        ob_clean();

        foreach ($files as $file) {
            if (is_file($file)) {
                require_once $file;
                $content = ob_get_contents();
                ob_clean();
                return $content;
            }
        }

        return View::getTemplate('default');
    }

    static function get($view, $data = [], $template = 'default')
    {
        $path = \PATH . "app/views/";
        $filename = str_replace("/", DIRECTORY_SEPARATOR, "{$path}/{$view}");
        
        // $Auth = Auth::getUser();
        // if($Auth===false)
        //     $user = false;
        // else
        //     $user = $Auth->toArray();

        $files = [
            $filename,
            $filename.".php",
            $filename.".html"
        ];
        
        extract($data);

        ob_start();
        foreach ($files as $file) {
            if (is_file($file)) {
                require_once $file;
                return View::getTemplate($template);
            }
        }

        throw new \Exception("Arquivo não encontrado!");
    }
}

