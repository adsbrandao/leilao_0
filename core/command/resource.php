CREATE RESOURCE ---

<?php
use Core\StringCase;

$pathControllers = 'app/Controllers/';
$namespaceControllers = 'App\\Controllers';

$controller = StringCase::pascalCase($params['controller'] ?? $params[0]);
$model = StringCase::pascalCase($params['model'] ?? $params[1]);

if (substr($controller, -10) != 'Controller') $controller .= "Controller";

$file = $pathControllers . $controller . '.php';
if (is_file(PATH . $file)) {
    echo "ERROR: Controller não existe.\n";
    exit;
}

$controller = explode('/', $controller);
$controllerName = array_pop($controller);

if (count($controller) > 0) {
    $namespaceControllers .= "\\".implode("\\", $controller);
}
$controller = $controllerName;

$content = "<?php namespace {$namespaceControllers};\n\n".
    "use Core\\ControllerResource;\n".
    "use Models\\{$model};\n\n".
    "class {$controller} extends ControllerResource {\n\n";
$content .= "    public function __construct () {\n".
    "        \$this->setModel (new {$model}());\n".
    "    }\n".
    "    // public function index () {}\n".
    "    // public function show (\$id) {}\n".
    "    // public function create () {}\n".
    "    // public function edit (\$id) {}\n".
    "    // public function store () {}\n".
    "    // public function update (\$id) {}\n".
    "    // public function delete (\$id)\n".
    "}\n";

echo "SAVE {$file}\n\n";
file_put_contents(PATH . $file, $content);
