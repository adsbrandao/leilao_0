<?php

use Core\Query;
use Core\StringCase;

echo "GENERATE MODELS\n";
$tables = Query::getTables();

$pathModel = 'app/models/';
$pathBase = $pathModel . 'Base/';

if (is_dir(PATH . $pathModel)) {
    echo "Create DIR: {$pathModel}\n";
    mkdir(realpath(PATH . $pathModel));
}
if (is_dir(PATH . $pathBase)) {
    echo "Create DIR: {$pathBase}\n";
    mkdir(realpath(PATH . $pathBase));
}

$namespaceModel = 'Models';
$namespaceBase = $namespaceModel . '\\Base';

foreach ($tables as $table) {
    $prefix = substr($table, 0, 3);
    if ($prefix=='tbl') {
        $nameModel = substr($table, 3);
    } else {
        continue;
    }
    $nameModel = StringCase::pascalCase($nameModel);
    $nameBase = StringCase::pascalCase($nameModel)."Base";
    $fileModel = $pathModel.$nameModel . '.php';
    $fileBase = $pathBase.$nameBase . '.php';

    // INICIO BASE
    if (is_file($fileBase)) {
        echo "DELETE {$nameBase}\n";
        unlink($fileBase);
    }

    echo "CREATE {$nameBase}\n";
    $contentBase = "<?php namespace {$namespaceBase};\n\n".
    "use Core\Model;\n\n".
    "class {$nameBase} extends Model {\n\n";

    $fields = Query::getFields($table);
    $contentBase .= "    /**\n";
    foreach ($fields as $campo) {
        // echo "ADD {$campo}\n";
        $contentBase .= "    * @var mixed \${$campo};\n";
    }
    $contentBase .= "    */\n";

    $contentBase .= "\npublic function __construct (\$data = [])\n".
        "{\n".
        "    \$this->setTable('{$table}');\n".
        "    \$this->setPrimaryKey('{$fields[0]}');\n".
        "    parent::__construct(\$data);\n".
        "}\n".
        "}\n";

    echo "SAVE {$fileBase}\n\n";
    file_put_contents(PATH . $fileBase, $contentBase);
    // FIM BASE

    // INICIO MODEL
    if (!is_file(PATH . $fileModel)) {
        echo "CREATE {$nameModel}\n";
        $contentModel = "<?php namespace {$namespaceModel};\n\n".
        "use {$namespaceBase}\\{$nameBase};\n\n".
        "class {$nameModel} extends {$nameBase} {\n".
        "}\n";
        echo "SAVE {$fileModel}\n\n";
        file_put_contents(PATH . $fileModel, $contentModel);
    }
    // FIM MODEL
}
