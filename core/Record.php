<?php namespace Core;

class Record implements ModelInterface {
    private $_data = [];
    private $_model;
    private $_modelClass;
    public function __construct ($data = [])
    {
        $this->setData($data);
    }

    public function __set ($name, $value)
    {
        return $this->set($name, $value);
    }

    public function __get ($name)
    {
        return $this->get($name);
    }

    public function __isset ($name)
    {
        return $this->isset($name);
    }

    public function set ($name, $value)
    {
        $this->_data[$name] = $value;
    }

    public function get ($name)
    {
        if(isset($this->_data[$name])) {
            return $this->_data[$name];
        }

        return null;
    }

    public function isset ($name)
    {
        return isset($this->_data[$name]);
    }

    /**
     * Define Nome da class
     *
     * @param String $modelClass Nome da class
     * @return void
     */
    public function setModel ($modelClass)
    {
        $this->_modelClass = $modelClass;
    }

    /**
     *
     * @return ModelInterface
     */
    public function getModel ()
    {
        if (is_null($this->_model)) {
            return $this->_model;
        }
        if (is_null($this->_modelClass)) {
            throw new \Exception("Model não definido para o Record");
        }

        $file = str_replace("\\", '/', $this->_modelClass);
        $file = \PATH . str_replace("//", '/', $file) . '.php';
        $file = str_replace("\\", DIRECTORY_SEPARATOR) . $file;

        if (is_file($file)) require_once($file);

        $this->_model = new $this->_modelClass();
        return $this->_model;
    }

    public function setConnectionId($connectionsId)
    {
        return $this->getModel()->setConnectionId($connectionsId);
    }
    /**
     * Define a tabela do model
     *
     * @param String $tableName
     * @return void
     */
    public function setTable($tableName)
    {
        return $this->getModel()->setTable($tableName);

    }
    /**
     * Define o campo chave primary
     *
     * @param String $fieldName
     * @return void
     */
    public function setPrimaryKey($fieldName)
    {
        return $this->getModel()->setPrimaryKey($fieldName);

    }

    public function getTable()
    {
        return $this->getModel()->getTable();

    }
    public function getPrimaryKey()
    {
        return $this->getModel()->getPrimaryKey();

    }

    /**
     * Lista simples em array dos campos do model
     *
     * @return void
     */
    public function getFields()
    {
        return $this->getModel()->getFields();

    }
    /**
     * Executa INSERT INTO no banco
     *
     * @param Array $data
     * @return Int Novo Id
     */
    public function insert ($data = null)
    {
        return $this->getModel()->insert(is_null($data) ? $this->_data : $data);

    }
    /**
     * Executa UPDATE no banco
     *
     * @param Int $id
     * @param Array $data
     * @return void
     */
    public function update ($id, $data = null)
    {
        return $this->getModel()->update($id, is_null($data) ? $this->_data : $data);

    }
    public function setFieldDefault ($fieldName, $default = null, $permitNull = null)
    {
        return $this->getModel()->setFieldDefault($fieldName, $default, $permitNull);

    }
    public function setFields ($fields)
    {
        return $this->getModel()->setFields($fields);

    }
    public function select ($fields = '*')
    {
        return $this->getModel()->select($fields);

    }
    public function delete ($where, $order = null, $limit = 1)
    {
        return $this->getModel()->delete($where, $order, $limit);

    }

    /**
     * Busca registro pelo primaryKey
     *
     * @param Int|String|Array $where Valor do primaryKey, Array para outros parametros para o WHERE
     * @param Array $bindData BindData
     * @param String $order
     * @return this|Boolean False se não encontrar
     */
    public static function find ($where = null, $bindData = [], $order = null)
    {
        $record = new self();
        return $record->getModel()->find($where, $order);

    }

    /**
     * Busca registro pelo primaryKey
     *
     * @param Int|String|Array $where Valor do primaryKey, Array para outros parametros para o WHERE
     * @param Array $bindData BindData
     * @param String $order
     * @return array de Record
     */
    public static function findAll ($where = null, $bindData = [], $order = null, $limit = null)
    {
        $record = new self();
        return $record->getModel()->findAll($where, $order, $limit);

    }

    /**
     * Executa query sql
     *
     * @param String $sql QuerySql
     * @param array $bindData
     * @return Boolean|\PDOStatement False se erro
     */
    public static function query ($sql, $bindData = [])
    {
        $record = new self();
        return $record->getModel()->query($sql, $bindData);
    }


    /**
     * Executa query de select sql (fetchmode: class)
     *
     * @param String $sql QuerySql
     * @param array $bindData
     * @return Boolean|\PDOStatement False se erro
     */
    public static function querySelect ($sql, $bindData = [])
    {
        $record = new self();
        return $record->getModel()->query($sql, $bindData);
    }

    /**
     * Conta os registros da tabela
     *
     * @param Int|String|Array $where
     * @param Array $bindData
     * @param String $joins
     * @return Int|Boolean False se Erro
     */
    public static function sizeof ($where = null, $bindData = [], $joins = null)
    {
        $record = new self();
        return $record->getModel()->sizeof($where);
    }

    public function setData ($data)
    {
        $this->_data = $data;
    }

    public function dateFormat ($fieldName, $format = 'd/m/Y')
    {
        $value = $this->_data[$fieldName] ?? null;
        $value = strtotime($value);

        if (is_null($value) || $value < 100 || !$value) {
            return '';
        }
        return date($format, $value);
    }

    public function dateTimeFormat ($fieldName, $format = 'd/m/Y H:i:s')
    {
        return $this->dateFormat($fieldName, $format);
    }

    /**
     * Retorna dados como array
     *
     * @return Array
     */
    public function toArray()
    {
        return $this->_data;
    }
}