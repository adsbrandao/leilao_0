$(document).ready(function () {
    // $('input[data-mask').each(function () {
    //     $(this).mask();
    // });
    $('input.time').mask('00000-000');
    $('input.cpf').mask('000.000.000-00', {reverse: true});
    $('input.cnpj').mask('00.000.000/0000-00', {reverse: true});
    
    $('.money').mask('000.000.000.000.000,00', {
        reverse: true
    });

    var SPMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00)00000-0000' : '(00)0000-00009';
    };

    var spOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(SPMaskBehavior.apply({}, arguments), options);
        }
    };

    $('input.telefone').mask(SPMaskBehavior, spOptions);


    var cpfCnpjMaskBehavior = function (val) {
        return val.replace(/\D/g, '').length > 11 ? '00.000.000/0000-00' : '000.000.000-009';
    };

    var cpfCnpjOptions = {
        onKeyPress: function(val, e, field, options) {
            field.mask(cpfCnpjMaskBehavior.apply({}, arguments), options);
        }
    };
    $('input.cpfcnpj').mask(cpfCnpjMaskBehavior, cpfCnpjOptions);
    $('input.celular').mask('(00)00000-0000');

    $('input.cep').mask('00000-000');
});