
const showMsg = function (selector, msg, icon = '', type = 'primary') {
    if (msg == '') {
        $(selector).html('');
    } else {
        let html = `<div class="alert alert-${type}">`;
        if (icon != '') {
            html += `<i class="${icon}"></i> `;
        }
        html += msg + '</div>';
        $(selector).html(html);
        $(selector).unbind('click').click(function () {
            $(this).html('');
        });
    }
};

const sendForm = function (url, element, options = {}) {
    let formData = new FormData(element);

    options = Object.assign({
        method: 'POST',
        fncBefore: (formData, element) => formData,
        fncValid: (formData, element) => {
            return [];
        },
        fncOnInvalid: (errors, formData, element) => {}
    }, options);

    let errors = options.fncValid(formData, element);
    if (errors.length > 0) {
        options.fncOnInvalid(errors, formData, element);
        return Promise.reject(errors);
    }

    formData = options.fncBefore(formData, element);

    return fetch(url, {method: options.method, body: formData, mode: 'cors'})
        .then(res => res.json());
};

const http = function (url,method = 'GET', body = null, options = {}) {
options = Object.assign({method: 'GET', body: null, mode: 'cors'}, options);
options.body = body;
options.method = method;

return fetch(url, options);
};

const httpJSON = function (url,method = 'GET', body = null, options = {}) {
    return http(url,method, body, options)
        .then(res => {
            if (res.ok) {
                return res.json();
            }
            return res;
        });
};

const consultaCep = (cep) => {
    cep = String(cep).replace(/[^0-9]/g, '');

    return fetch('http://cep.republicavirtual.com.br/web_cep.php?cep=' + cep + '&formato=json')
        .then(res => res.json())
};

const onChangeCep = (value, campos = {}) => {
    campos = Object.assign({
        cep: '#cep,#f_cep',
        endereco: '#endereco,#f_endereco',
        numero: '#numero,#f_numero',
        complemento: '#complemento,#f_complemento',
        bairro: '#bairro,#f_bairro',
        cidade: '#cidade,#f_cidade',
        estado: '#estado,#f_estado',
    }, campos);

    value = String(value).replace(/[^0-9]/g, '');
    if (value.length != 8) return false;

    consultaCep(value).then(res => {
        let endereco = (typeof res.tipo_logradouro != 'undefined' ? res.tipo_logradouro : '');
        endereco += (endereco=='' ? '' : ' ') + ((typeof res.logradouro != 'undefined' ? res.logradouro : ''));
        $(campos.endereco).val( endereco );
        $(campos.bairro).val( (typeof res.bairro != 'undefined' ? res.bairro : '') );
        $(campos.cidade).val( (typeof res.cidade != 'undefined' ? res.cidade : '') );
        $(campos.estado).val( (typeof res.uf != 'undefined' ? res.uf : '') ); 
        $(campos.numero).focus();
    });
}

const testCPF = (cpf) => {
    cpf = String(cpf).replace(/[^0-9]/g, '');
       
    const nonNumbers = /\D/;
    
    if (cpf.length!=11 || nonNumbers.test(cpf)) return false;

    if (cpf == "00000000000" ||
        cpf == "11111111111" ||
        cpf == "22222222222" ||
        cpf == "33333333333" || 
        cpf == "44444444444" ||
        cpf == "55555555555" ||
        cpf == "66666666666" ||
        cpf == "77777777777" ||
        cpf == "88888888888" || 
        cpf=="99999999999") {
        return false;
    }

    let a = [];
    let b = new Number;
    let c = 11;
    for(var i=0;i<11;i++) {
        a[i] = cpf.charAt(i);
        if(i<9) b += (a[i]* --c);
    }
    if((x=b%11)<2) {
        a[9]=0;
    } else {
        a[9]=11-x;
    }
    b=0;
    c=11;
    for(var y=0;y<10;y++) {
        b+=(a[y]*c--);
    }
    if((x=b%11)<2) {
        a[10]=0;
    } else {
        a[10]=11-x;
    }

    return (cpf.charAt(9)==a[9]) && (cpf.charAt(10)==a[10]);
};

const testCNPJ = (cnpj) => {
    cnpj = String(cnpj).replace(/[^0-9]/g, '');

    const nonNumbers = /\D/;
    if (cnpj.length != 14 || nonNumbers.test(cnpj)) return false;

    if(
        cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999" ) {
        return false;
    }
    let numeroAtual = new Number;
    let d1 = new Number;
    let d2 = new Number;
    for(var i=0;i<12;i++){
        numeroAtual = cnpj.charAt(11-i);
        d1 += numeroAtual * ((i<8)?(i+2):(i-6));
        d2 += numeroAtual * ((i<7)?(i+3):(i-5));
    }
    d1 = 11-(d1%11);
    if(d1>=10) d1=0;
    d2 += d1*2;
    d2 = 11-(d2%11);
    if(d2>=10) d2=0;

    return (cnpj.charAt(12)==d1) && (d2==cnpj.charAt(13));
};

const testeCadastro = (id, email, cpf, cnpj, fncSuccess = (msg) => { console.log(msg) }, fncError = (msg) => { console.error(msg) }) => {
    let body = new FormData();

    body.append('id', id);
    body.append('email', email);
    body.append('cpf', cpf);
    body.append('cnpj', cnpj);
    httpJSON(baseURL + 'teste-cadastro', 'POST', body)
        .then(res => {
            let ok = res.ok??true;
            let status = (!ok)? false : (res.status??false);
            
            if (!ok) {
                fncError('Erro ao consultar cadastro');
            } else if(status) {
                fncSuccess(res.message);
            } else {
                fncError(res.message);
            }
        }).catch(reason => {
            fncError(reason);
        })
};
