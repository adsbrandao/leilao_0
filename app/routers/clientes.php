<?php

use Core\Router;

Router::get('index','/minha-area', 'AreaCliente/Dashboard@index');

// Minha conta
Router::get('minha_conta','/minha-area/conta', 'AreaCliente/Conta@editar');
Router::post('minha_conta_update','/minha-area/conta', 'AreaCliente/Conta@update');

// Leiloes
// Meus Arremates
Router::get('arremates','/minha-area/arremates', 'AreaCliente/Arremates@index');
