<?php namespace App\Controllers\AreaCliente;

use Core\Controller;
use Core\View;
use Core\Auth;
use Models\Leilao;

class DashboardController extends Controller {
    public function index () {
        $userData = Auth::get();

        $data = [
            'countAbertos' => Leilao::sizeof(['status' => Leilao::PUBLICADO]),
            'countAndamento' => Leilao::sizeof(['status' => Leilao::ANDAMENTO]),
            'countArremates' => Leilao::sizeof(['arrematado_por' => $userData->cliente_id,'status' => Leilao::ENCERRADO]),
            'contaStatus' => null,
            'leiloesAbertos' => Leilao::findAll(['status' => Leilao::PUBLICADO], [], "id DESC", 10),
            'leiloesAndamento' => Leilao::findAll(['status' => Leilao::ANDAMENTO], [], "id DESC", 10),
            'leiloesArremates' => Leilao::findAll(['arrematado_por' => $userData->cliente_id,'status' => Leilao::ENCERRADO], [], "id DESC", 10)
        ];

        if ($userData->cliente_status == 1) {
            $data['contaStatus'] = 'Ativo';
        } elseif ($userData->cliente_status == 2) {
            $data['contaStatus'] = 'Aguandando ativação';
        } else {
            $data['contaStatus'] = 'Inativo';
        }

        return View::get('area-cliente/dashboard', $data);
    }
}
