<?php namespace App\Controllers\AreaCliente;

use Core\{Controller, Auth, View};
use Models\Cliente;

class ContaController extends Controller {

    public function index () {
        return 'conta';
    }

    public function editar () {
        $idCliente = Auth::getIdCliente();

       $data = Cliente::find($idCliente);
        return View::get('conta/editar', [ "data"=>$data ]);
    }

    public function update () {
        $response = ['status' => 1, 'message' => 'Cadastrado com sucesso'];
        try {
        
            $idCliente = Auth::getIdCliente();
            $cliente = new Cliente();

            $dados = [
                'nome' => $_POST ['nome']?? NULL,
                'sobrenome' => $_POST ['sobrenome']?? NULL,
                'telefone' => $_POST ['telefone']?? NULL,
                'celular' => $_POST ['celular']?? NULL,
                'email' => $_POST ['email']?? NULL,
                'nascimento' => $_POST ['nascimento']?? NULL,
                'razao_social' => $_POST ['razao_social']?? NULL,
                'cep' => $_POST ['cep']?? NULL,
                'endereco' => $_POST ['endereco']?? NULL,
                'numero' => $_POST ['numero']?? NULL,
                'complemento' => $_POST ['complemento']?? NULL,
                'bairro' => $_POST ['bairro']?? NULL,
                'estado' => $_POST ['estado']?? NULL,
                'cidade' => $_POST ['cidade']?? NULL,
                'data_update' => date('Y-m-d H-i-s')
            ];

            $senha = $_POST['senha'] ?? '';
            if ($senha != '') {
                $dados['senha'] = md5($senha);
            }

            $cliente->update($idCliente, $dados);
        } catch (\Exception $e) {
                $response = ['status' => 0, 'message' => $e->getMessage(), 'trace' => $e->getTrace()];
            }   
        return $response;
    }
}
