<?php namespace App\Controllers\Admin;

use Core\Auth;
use Core\Controller;
use Core\View;
use Models\Leilao;
use Models\Cliente;

class DashboardController extends Controller {
    public function index () {
        $userData = Auth::get();

        $data = [
            'countAbertos' => Leilao::sizeof(['status' => Leilao::PUBLICADO]),
            'countAndamento' => Leilao::sizeof(['status' => Leilao::ANDAMENTO]),
            'countArremates' => Leilao::sizeof(['status' => Leilao::ENCERRADO]),
            'countClientes' => Cliente::sizeof(['status' => 1]),
            'leiloesAbertos' => Leilao::findAll(['status' => Leilao::PUBLICADO], [], "id DESC", 10),
            'leiloesAndamento' => Leilao::findAll(['status' => Leilao::ANDAMENTO], [], "id DESC", 10),
            'novosClientes' => Cliente::findAll(['status' => 2], [], "id DESC", 10)
        ];

        return View::get('admin/dashboard', $data);
    }
}
