<?php namespace App\Controllers\Admin;

use Core\Controller;
use Core\ResponseSimple;
use Models\LeilaoFoto;

class LeilaoFotoController extends Controller {
    public static function limpar ()
    {
        if (isset($_SESSION['leilaoFoto'])) {
            foreach ($_SESSION['leilaoFoto'] as $foto) {
                if (is_file($foto['path_original'])) unlink($foto['path_original']);
                if (is_file($foto['path_thumb'])) unlink($foto['path_thumb']);
            }
        }
        $_SESSION['leilaoFoto'] = [];
    }
    public function readTmp ($key, $tipo = '')
    {
        try {
            $imagem = '';

            if (isset($_SESSION['leilaoFoto'][$key])) {
                if (is_file($_SESSION['leilaoFoto'][$key]['path_original'])) {
                    $imagem = $_SESSION['leilaoFoto'][$key]['path_original'];
                }
                if ($tipo == 'thumb' && is_file($_SESSION['leilaoFoto'][$key]['path_thumb'])) {
                    $imagem = $_SESSION['leilaoFoto'][$key]['path_thumb'];
                }
            }

            if ($imagem == '') {
                return new ResponseSimple('', 404);
            }

            \ob_clean();
            header('Content-type:' . filetype($imagem));
            readfile($imagem);
            exit;
        } catch (\Exception $e) {
            return new ResponseSimple('', 404);
        }
    }

    private static function getFotos ($leilaoId, $showPath = false)
    {
        $fotos = LeilaoFoto::findAll(['leilao_id' => $leilaoId], [], 'ordem ASC');

        $data = [];

        if ($fotos != false) {
            $data = $fotos->fetchAll(\PDO::FETCH_ASSOC);
            $i = 0;
            foreach($data as $key => $foto) {
                $data[$key]['key'] = $i++;
                $data[$key]['imagem_original'] = \URL.'upload/'.$foto['imagem_original'];
                $data[$key]['imagem_thumb'] = \URL.'upload/'.$foto['imagem_thumb'];

                $data[$key]['path_original'] = $showPath? (\PATH.'upload/'.$foto['imagem_original']) : '';
                $data[$key]['path_thumb'] = $showPath? (\PATH.'upload/'.$foto['imagem_thumb']) : '';
            }
        }

        if (isset($_SESSION['leilaoFoto'])) {
            $nFotos = count($data);
            foreach ($_SESSION['leilaoFoto'] as $i=>$foto) {
                $key = $i + $nFotos - 1;
                $data[] = [
                    'id' => 0,
                    'key' => $key,
                    'leilao_id' => $leilaoId,
                    'ordem' => 0,
                    'ext' => $foto['ext'],
                    'imagem_original' => \URL.'admin/leilao-foto/tmp/'.$i.'/original',
                    'imagem_thumb' => \URL.'admin/leilao-foto/tmp/'.$i.'/thumb',
                    'path_original' => $showPath?$foto['path_original']:'',
                    'path_thumb' => $showPath? $foto['path_thumb'] : ''
                ];
            }
        }

        return $data;
    }

    public function fotosLeilao ($leilaoId = 0)
    {
        try {
            return ['status' => true, 'message' => '', 'data' => $this->getFotos($leilaoId)];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }

    public function upload ($leilaoId = 0) {
        try {
            if (isset($_FILES['arquivo']['tmp_name'])) {
                foreach ($_FILES['arquivo']['tmp_name'] as $key => $tmpName) {
                    if (is_uploaded_file($tmpName)) {
                        $name = $_FILES['arquivo']['name'][$key];
                        $ext = preg_replace('/^.*[\.]([^\.]*)$/', '$1', $name);
                        $pathArquivo = \PATH.'tmp/leilao-'.$leilaoId.time();
                        $pathArquivoThumb = $pathArquivo.'t.'.$ext;
                        $pathArquivo = $pathArquivo.'.'.$ext;

                        $extPermitidas = [
                            'jpg',
                            'jpeg',
                            'gif',
                            'png',
                            'svg',
                            'webp'
                        ];

                        if (!in_array($ext, $extPermitidas)) {
                            throw new \Exception("Envie somente imagens do tipo: jpg, png ou gif");
                        }
                        
                        move_uploaded_file($tmpName, $pathArquivo);
                        
                        if (!isset($_SESSION['leilaoFoto'])) {
                            $_SESSION['leilaoFoto'] = [];
                        }

                        // Reduzir tamanho de imagem
                        \Core\Commons::loadLib('WideImage');
                        $image = \WideImage::load($pathArquivo);
                        $resized = $image->resize(1000, null, 'inside');
                        if ($ext == 'jpg') {
                            $resized->saveToFile($pathArquivo, 80);
                        } elseif ($ext == 'png') {
                            $resized->saveToFile($pathArquivo, 6);
                        } else {
                            $resized->saveToFile($pathArquivo);
                        }

                        // Gerar Thumb
                        $resized = $image->resize(600, null, 'inside')->crop('center', 'center', 600, 400);
                        if ($ext == 'jpg') {
                            $resized->saveToFile($pathArquivoThumb, 80);
                        } elseif ($ext == 'png') {
                            $resized->saveToFile($pathArquivoThumb, 6);
                        } else {
                            $resized->saveToFile($pathArquivoThumb);
                        }

                        $_SESSION['leilaoFoto'][] = [
                            'ext' => $ext,
                            'path_original' => $pathArquivo,
                            'path_thumb' => $pathArquivoThumb
                        ];

                    } elseif($_FILES['arquivo']['error'][$key]!=\UPLOAD_ERR_OK) {
                        switch ($_FILES['arquivo']['error'][$key]) {
                            case \UPLOAD_ERR_INI_SIZE:
                            case \UPLOAD_ERR_FORM_SIZE:
                                throw new \Exception("Tamanho do arquivo muito grande");
                                break;
                            case \UPLOAD_ERR_CANT_WRITE:
                                throw new \Exception("Erro ao salvar foto");
                                break;
                            case \UPLOAD_ERR_EXTENSION:
                                throw new \Exception("Envie somente imagens do tipo: jpg, png, ou gif");
                                break;
                            case \UPLOAD_ERR_NO_FILE:
                                break;
                            default:
                                throw new \Exception("Erro ao enviar a foto");
                                break;
                        }
                    }
                }
            }

            return new ResponseSimple($this->getFotos($leilaoId), 200);
        } catch (\Exception $e) {
            return new ResponseSimple(['status' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public static function save ($leilaoId)
    {
        try {
            $model = new LeilaoFoto();
            $data = $_POST['fotos']??'[]';

            $data = json_decode($data, true);

            if (json_last_error() != JSON_ERROR_NONE) {
                throw new \Exception("Erro ao salvar fotos do leilão.");
            }

            $fotos = self::getFotos($leilaoId, true);

            foreach ($fotos as $key=>$foto) {
                $deleted = $data[$key]['deleted'] ?? 0;
                $deleted = $deleted === 1 || $deleted === true || $deleted === 'true';

                $ordem = $data[$key]['ordem'] ?? $key;

                if ($foto['id'] == 0) {
                    $foto['leilao_id'] = $leilaoId;
                    if (!$deleted) {
                        $posfix = $leilaoId . '-'.$key.'-'.time().'.'.$foto['ext'];
                        $fileOriginal = 'leilao-'.$posfix;
                        $fileThumb = 'leilao-t-'.$posfix;

                        copy($foto['path_original'], \PATH.'upload/'.$fileOriginal);
                        copy($foto['path_thumb'], \PATH.'upload/'.$fileThumb);

                        $model->insert([
                            'leilao_id' => $leilaoId,
                            'ordem' => $ordem,
                            'imagem_original' => $fileOriginal,
                            'imagem_thumb' => $fileThumb
                        ]);
                    }
                } elseif ($deleted) {
                    if (is_file(\PATH.'upload/'. $foto['imagem_original'])) {
                        unlink(\PATH.'upload/'. $foto['imagem_original']);
                    }

                    if (is_file(\PATH.'upload/'. $foto['imagem_thumb'])) {
                        unlink(\PATH.'upload/'. $foto['imagem_thumb']);
                    }
                    
                    $model->delete($foto['id']);
                } else {
                    $model->update($foto['id'], [
                        'ordem' => $ordem
                    ]);
                }
            }

            self::limpar();

            return ['status' => true, 'message' => '', 'data' => $data];
        } catch (\Exception $e) {
            return ['status' => false, 'message' => $e->getMessage()];
        }
    }
}