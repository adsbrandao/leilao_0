<?php namespace App\Controllers\Admin;

use Core\ControllerResource;
use Core\Auth;
use Core\View;
use Core\ResponseSimple;
use Models\Leilao;
use Models\Usuario;
use Models\Cliente;
use Models\LeilaoFoto;
use Models\Lance;

class LeilaoController extends ControllerResource {

    public function __construct () {
        $this->setTitle('Leilões');
        $this->setBaseUrl(URL.'admin/leilao');
        $this->setModel(new Leilao());
    }

    public function create ()
    {

        $model = $this->getModel();
        $data = [
            'title' => $this->getTitle(),
            'baseURL' => $this->getBaseUrl(),
            'primaryKey' => $model->getPrimaryKey(),
            'id' => 0,
            'fields' => $this->getFieldsForm(0),
            'js' => $this->getJSForm(),
            'jsOnload' => $this->getJSFormLoad(),
            'data' => []
        ];

        return View::get('admin/leilao_edit', $data);
    }
    
    public function edit ($id)
    {
        $model = $this->getModel();
        $res = $model->find($id);

        if (!$res) {
            return new ResponseSimple("Conteúdo não encontrado", 404);
        }

        $data = [
            'title' => $this->getTitle(),
            'baseURL' => $this->getBaseUrl(),
            'primaryKey' => $model->getPrimaryKey(),
            'id' => $id,
            'fields' => $this->getFieldsForm($id),
            'js' => $this->getJSForm(),
            'jsOnload' => $this->getJSFormLoad(),
            'data' => $res->toArray()
        ];
        return View::get('admin/leilao_edit', $data);
    }

    public function setColumnDataTable ($dataTable)
    {
        $model = $this->getModel();
        $fields = $model->getFields();
        $table = $model->getTable();

        $this->addColumnAction($dataTable);

        // $this->addColumnDataTable(
        //     $dataTable,
        //     'id',
        //     $table . '.id',
        //     'ID',true, true,null, 1
        // );

        $this->addColumnDataTable(
            $dataTable,
            'lote',
            $table . '.lote',
            'Lote',true, true,null, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'foto_thumb',
            $table . '.id',
            'Foto',true, true,function ($v) {
                $foto = LeilaoFoto::find(['leilao_id' => $v], [], 'ordem ASC');
                if ($foto!=false && is_file(\PATH.'upload/'.$foto->imagem_thumb)) {
                    return '<img src="'.\URL.'upload/'.$foto->imagem_thumb.'" style="max-width: 50px;max-height: 50px;" />';
                 } else {
                    return '<img src="'.\URL.'theme/img/sem-foto.jpg" style="opacity:0.3;max-width: 50px;max-height: 50px;" />';
                 }
            }, 1
        );

                
        $this->addColumnDataTable(
            $dataTable,
            'nome',
            $table . '.nome',
            'Nome'
        );

        $this->addColumnDataTable(
            $dataTable,
            'status',
            $table . '.status',
            'Status',true, true,function ($v, $rowData) {
                $valores = [
                    'Não publicado',
                    'Publicado (Aberto)',
                    'Em andamento',
                    'Encerrado',
                    'Cancelado',
                    'Apregoando'
                ];
                if(isset($valores[$v])) {
                    return $valores[$v];
                } else {
                    return '-';
                }
            }, 1
        );// 

        $this->addColumnDataTable(
            $dataTable,
            'destaque',
            $table . '.destaque',
            'Destaque',true, true,function ($v, $rowData) {
                $valores = [
                    '<nobr>Não destacado</nobr>',
                    'Destacado'
                ];
                if(isset($valores[$v])) {
                    return $valores[$v];
                } else {
                    return '-';
                }
            }
        );

        // $this->addColumnDataTable(
        //     $dataTable,
        //     'parcelado',
        //     $table . '.parcelado',
        //     'Parcelado',true, true,function ($v, $rowData) {
        //         $valores = [
        //             '<nobr>Não parcelado</nobr>',
        //             'Parcelado'
        //         ];
        //         if(isset($valores[$v])) {
        //             return $valores[$v];
        //         } else {
        //             return '-';
        //         }
        //     }
        // );

        $this->addColumnDataTable(
            $dataTable,
            'categoria_id',
            $table . '.categoria_id',
            'Categoria',true, true,function ($v, $rowData) {
                if (is_null($v)) {
                    return '-';
                }
                $res = \Models\LeilaoCategoria::find($v);
                if(!$res) {
                    return '-';
                } else {
                    return $res->nome;
                }
            }, 1
        );

        
        // $this->addColumnDataTable(
        //     $dataTable,
        //     'processo',
        //     $table . '.processo',
        //     'Descrição'
        // );
        
        $this->addColumnDataTable(
            $dataTable,
            'valor_inicial',
            $table . '.valor_inicial',
            'Valor inicial',true, true,function ($v, $rowData) {
                if (is_null($v)) {
                    return '-';
                } else {
                    return number_format($v,2,',','.');
                }
            }
        );
        
        // $this->addColumnDataTable(
        //     $dataTable,
        //     'salto_lance',
        //     $table . '.salto_lance',
        //     'Valor incrementado'
        // );
        
        $this->addColumnDataTable(
            $dataTable,
            'valor_arrematado',
            $table . '.valor_arrematado',
            'Valor arrematado',true, true,function ($v, $rowData) {
                if (is_null($v)) {
                    return '-';
                } else {
                    return number_format($v,2,',','.');
                }
            }
        );

        $this->addColumnDataTable(
            $dataTable,
            'data_create',
            $table . '.data_create',
            'Criação',
            true, true,
            function ($value, $dataRow) {
                $value = strtotime($value);
                if (!$value || is_null($value) || $value < 100) {
                    $value = '-';
                } else {
                    $value = date('d/m/Y H:i:s', $value);
                }
                return '<nobr>'.$value.'</nobr>';
            },
            1
        );

        $this->addColumnDataTable(
            $dataTable,
            'data_inicio',
            $table . '.data_inicio',
            'Início',true, true,function ($v, $rowData) {
                if (is_null($v)) {
                    return '-';
                } else {
                    return date('d/m/Y H:i:s', strtotime($v));
                }
            }, 1
        );

        $this->addColumnDataTable(
            $dataTable,
            'data_fim',
            $table . '.data_fim',
            'Fim',true, true,function ($v, $rowData) {
                if (is_null($v)) {
                    return '-';
                } else {
                    return date('d/m/Y H:i:s', strtotime($v));
                }
            }, 1
        );

        return $dataTable;
    }

    public function getFieldsForm ($id)
    {
        $model = $this->getModel();

        if ($id > 0) {
            $leilao = $model->find($id);
        } else {
            $leilao = false;
        }

        $i = 0;

        $fieldsModel = $model->getFields();
        $fields = [];

        $fields[$i++] = $fieldsModel['id'];

        // ---------------------------
        $fields[$i] = $fieldsModel['status'];
        $fields[$i]['label'] = 'Status';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['options'] = [
            ['value' => 0, 'label' => 'Não publicado'],
            ['value' => 1, 'label' => 'Publicado (Aberto)'],
            ['value' => 2, 'label' => 'Em andamento'],
            ['value' => 3, 'label' => 'Encerrado'],
            ['value' => 4, 'label' => 'Cancelado'],
            ['value' => 5, 'label' => 'Apregoando']
        ];
        $fields[$i]['default'] = 0;
        // $fields[$i]['maxlen'] = 255;
        // $fields[$i]['precision'] = 0;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['categoria_id'];
        $fields[$i]['label'] = 'Categoria';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['options'] = [];
        $fields[$i]['default'] = null;

        $resCategorias = \Models\LeilaoCategoria::findAll([], 'nome ASC');
        while($categoria = $resCategorias->fetch()) {
            $fields[$i]['options'][] = ['value' => $categoria->id, 'label' => $categoria->nome];
        }

        // $fields[$i]['maxlen'] = 255;
        // $fields[$i]['precision'] = 0;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['nome'];
        $fields[$i]['label'] = 'Nome';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 100;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['lote'];
        $fields[$i]['label'] = 'Lote';
        $fields[$i]['classCol'] = 'col-md-1 col-xs-12';
        $fields[$i]['native_type'] = 'String';
        $fields[$i]['default'] = '';
        $fields[$i]['maxlen'] = 100;
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['salto_lance'];
        $fields[$i]['label'] = 'Valor por lance';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'LONG';
        $fields[$i]['precision'] = 2;
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['valor_inicial'];
        $fields[$i]['label'] = 'Valor inicial';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'LONG';
        $fields[$i]['precision'] = 2;
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['data_inicio'];
        $fields[$i]['label'] = 'Data início do leilão';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'DATETIME';
        $fields[$i]['default'] = date('Y-m-d H:i:s');
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['data_fim'];
        $fields[$i]['label'] = 'Data fim do leilão';
        $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
        $fields[$i]['native_type'] = 'DATETIME';
        $fields[$i]['default'] = date('Y-m-d H:i:s');
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['destaque'];
        $fields[$i]['label'] = 'Destaque';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['options'] = [
            ['value' => 0, 'label' => 'Não'],
            ['value' => 1, 'label' => 'Sim']
        ];
        $fields[$i]['default'] = '0';
        $i++;
        
           // ---------------------------
        $fields[$i] = $fieldsModel['parcelado'];
        $fields[$i]['label'] = 'Parcelado';
        $fields[$i]['classCol'] = 'col-md-2 col-xs-12';
        $fields[$i]['native_type'] = 'SELECT';
        $fields[$i]['options'] = [
            ['value' => 0, 'label' => 'Não'],
            ['value' => 1, 'label' => 'Sim']
        ];
        $fields[$i]['default'] = '0';
        $i++;

        if ($leilao != false && $leilao->status == 3) {
            // Caso encerrado e ultimo lance ser do leiloeiro
            $lance = Lance::find(['leilao_id' => $id], [], 'id DESC');

            if (!$lance) {
                // Nenhum Lance
                // ---------------------------
                $fields[$i] = $fieldsModel['arrematado_por'];
                $fields[$i]['name'] = 'arrematado_por_nome';
                $fields[$i]['label'] = 'Arrematado por';
                $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
                $fields[$i]['native_type'] = 'READONLY';
                $fields[$i]['default'] = 'Nenhum Lance';
                $i++;
            } elseif(is_null($lance->cliente_id)) {
                // Ultimo lance pelo leiloeiro
                // ---------------------------
                $fields[$i] = $fieldsModel['arrematado_por'];
                $fields[$i]['label'] = 'Arrematado por';
                $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
                $fields[$i]['native_type'] = 'SELECT_AJAX';
                $fields[$i]['options'] = [
                    'url' => \URL."admin/cliente/ativos",
                    'default' => null
                ];

                if (!is_null($leilao->arrematado_por)) {
                    $cliente = Cliente::find($leilao->arrematado_por, [], 'id DESC');
                    if ($cliente->cnpj != '') {
                        $nome = $cliente->id.'- '.$cliente->razao_social;
                    } else {
                        $nome = $cliente->id.'- '.$cliente->nome.' '.$cliente->sobrenome;
                    }
                    $fields[$i]['options']['default'] = [
                        'value' => $cliente->id,
                        'label' => $nome
                    ];
                }
                $fields[$i]['default'] = null;
                $i++;
            } else {
                // Ultimo lance pelo cliente
                $cliente = Cliente::find($lance->cliente_id, [], 'id DESC');

                // ---------------------------
                $fields[$i] = $fieldsModel['arrematado_por'];
                $fields[$i]['name'] = 'arrematado_por_nome';
                $fields[$i]['label'] = 'Arrematado por';
                $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
                $fields[$i]['native_type'] = 'READONLY';
                $fields[$i]['default'] = $cliente->id . '- '. $cliente->nome;
                $i++;
            }


            // ---------------------------
            $fields[$i] = $fieldsModel['valor_arrematado'];
            $fields[$i]['label'] = 'Valor Arrematado';
            $fields[$i]['classCol'] = 'col-md-3 col-xs-12';
            $fields[$i]['native_type'] = 'READONLY';
            $fields[$i]['default'] = $lance->valor;
            $i++;
        }

        // ---------------------------
        $fields[$i] = $fieldsModel['processo'];
        $fields[$i]['label'] = 'Dados do processo';
        $fields[$i]['classCol'] = 'col-md-12 col-xs-12';
        $fields[$i]['native_type'] = 'TEXT';
        $fields[$i]['default'] = '';
        $i++;

        // ---------------------------
        $fields[$i] = $fieldsModel['descricao'];
        $fields[$i]['label'] = 'Descrição';
        $fields[$i]['classCol'] = 'col-md-12 col-xs-12';
        $fields[$i]['native_type'] = 'TEXT';
        $fields[$i]['default'] = '';
        $i++;

        // // ---------------------------
        // $fields[$i] = ['name' => 'separador'];
        // $fields[$i]['label'] = 'Imagens';
        // $fields[$i]['classCol'] = 'clearfix';
        // $fields[$i]['native_type'] = 'SEPARADOR';
        // $i++;

        // // ---------------------------
        // $fields[$i] = $fieldsModel['foto_original'];
        // $fields[$i]['label'] = 'Foto';
        // $fields[$i]['classCol'] = 'col-md-6 col-xs-12';
        // $fields[$i]['native_type'] = 'FILE';
        // $fields[$i]['default'] = '';
        // $fields[$i]['maxlen'] = 100;
        // $i++;

        // "data_create": "2021-03-05 11:01:15",
        // "data_update": "2021-03-05 11:01:15",
        // "data_acesso": "2021-03-05 11:01:15",
        // "imagem": null,

        return $fields;

    }
    public function beforeSave (&$data, $id = null)
    {
        if (is_null($id)) {
            $data['data_create'] = date('Y-m-d H:i:s', time());
        }
        $data['data_update'] = date('Y-m-d H:i:s', time());
        
        if ($data['status'] == 2) {
            $data['usuario_inicio'] = Auth::getIdUsuario();
        } elseif ($data['status'] == 3) {
            $data['usuario_fim'] = Auth::getIdUsuario();
        }

        // if (isset($_FILES['foto_original'])) {
        //     if (is_uploaded_file($_FILES['foto_original']['tmp_name'])) {
        //         $name = $_FILES['foto_original']['name'];
        //         $ext = preg_replace('/^.*([\.][^\.]*)$/', '$1', $name);

        //         if (!is_null($id)) {
        //             $res = Leilao::find($id);
        //             if ($res != false && is_file(\PATH.'upload/'.$res->foto_original)) {
        //                 @unlink(\PATH.'upload/'.$res->foto_original);
        //             }
        //         }
        //         $filename = "leilao-".($id ??'-').time().$ext;
        //         $data['foto_original'] = $filename;
        //         $data['foto_thumb'] = $filename;
                
        //         $extPermitidas = [
        //             '.jpg','.jpeg','.gif', '.png','.svg','.webp'
        //         ];
        //         if (!in_array($ext, $extPermitidas)) {
        //             throw new \Exception("Envie somente imagens do tipo: jpg, png ou gif");
        //         }
                
        //         move_uploaded_file($_FILES['foto_original']['tmp_name'], \PATH.'upload/'.$filename);
        //     } elseif($_FILES['foto_original']['error']!=\UPLOAD_ERR_OK) {
        //         switch ($_FILES['foto_original']['error']) {
        //             case \UPLOAD_ERR_INI_SIZE:
        //             case \UPLOAD_ERR_FORM_SIZE:
        //                 throw new \Exception("Tamanho do arquivo muito grande");
        //                 break;
        //             case \UPLOAD_ERR_CANT_WRITE:
        //                 throw new \Exception("Erro ao salvar foto");
        //                 break;
        //             case \UPLOAD_ERR_EXTENSION:
        //                 throw new \Exception("Envie somente imagens do tipo: jpg, png, ou gif");
        //                 break;
        //             case \UPLOAD_ERR_NO_FILE:
        //                 unset($data['foto_original']);
        //                 break;
        //             default:
        //                 throw new \Exception("Erro ao enviar a foto");
        //                 break;
        //         }
        //     }
        // } else {
        //     unset($data['foto_original']);
        // }
    }

    public function afterSave ($data, $id)
    {
        LeilaoFotoController::save($id);
    }

    public function show ($id)
    {
        $output = $_GET['output']??'html';
        $httpCode = 200;
        $model = $this->getModel();

        $res = $model->find($id);

        if (!$res) {
            return new ResponseSimple("Conteúdo não encontrado", 404);
        }

        $resUsuarioInicio = is_null($res->usuario_inicio) ? false : Usuario::find($res->usuario_inicio);
        $resUsuarioFim = is_null($res->usuario_fim) ? false : Usuario::find($res->usuario_fim);
        $resCliente = is_null($res->arrematado_por) ? false : Cliente::find($res->arrematado_por);
        $resFotos = LeilaoFoto::findAll(['leilao_id' => $res->id], [], 'ordem ASC');

        if ($output == 'json') {
            $data = [
                'id' => $id,
                'data' => $res->toArray(),
                'fotos' => $resFotos->fetchAll(\PDO::FETCH_ASSOC),
                'usuarioInicio' => !$resUsuarioInicio ? false : $resUsuarioInicio->toArray(),
                'usuarioFim' => !$resUsuarioFim ? false : $resUsuarioFim->toArray(),
                'arrematadoPor' => !$resCliente ? false : $resCliente->toArray()
            ];

            return new ResponseSimple($data, $httpCode);
        }

        $data = [
            'id' => $id,
            'data' => $res,
            'fotos' => $resFotos,
            'usuarioInicio' => $resUsuarioInicio,
            'usuarioFim' => $resUsuarioFim,
            'arrematadoPor' => $resCliente
        ];

        return View::get('admin/leilao_show', $data);
    }

    public function delete ($id)
    {
        try {
            $model = $this->getModel();
            
            $res = $model->find($id);

            if ($res != false) {
                if ($res->status != 0) {
                    throw new \Exception("Não é possível deletar leilões publicados", 403);
                }
                $res->delete();
            }

            return new ResponseSimple('', 200);
        } catch (\Exception $e) {
            return new ResponseSimple($e->getMessage(), $e->getCode());
        }
    }
}
