<?php namespace App\Controllers;

use Core\Controller;
use Core\View;

class IndexController extends Controller {

    public function index () {
        return View::get('index');
    }
}
