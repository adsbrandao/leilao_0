<?php namespace Models;

use Core\Auth;
use Models\Base\UsuarioBase;

class Usuario extends UsuarioBase {
    static $_permissoes;
    /**
     * Pega lista de permissoes(id) do usuario logado
     * 
     *
     * @return Array Lista de ids das permissoes do grupo/usuario
     */
    public static function getPermissoes ()
    {
        try {
            if (!is_null(Usuario::$_permissoes)) return Usuario::$_permissoes;
            $dataUser = Auth::get();
            $idGrupo = Auth::getIdGrupo();

            if (!$dataUser) return [];

            $sql = "SELECT permissao_id FROM tblpermissao_grupo WHERE grupo_id = ?";

            $bindData = [$idGrupo];

            $res = PermissaoGrupo::query($sql, $bindData);

            if (!$res) return [];

            $permissoes = [];

            while(list($permissaoId) = $res->fetch(\PDO::FETCH_NUM)) {
                $permissoes[] = $permissaoId;
            }

            Usuario::$_permissoes = $permissoes;

            return $permissoes;
        } catch (\Exception $e) {
            return [];
        }
    }
}
