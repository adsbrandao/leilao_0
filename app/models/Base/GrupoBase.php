<?php namespace Models\Base;

use Core\Model;

class GrupoBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $nome;
    * @var mixed $status;
    */

public function __construct ($data = [])
{
    $this->setTable('tblgrupo');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
