<?php namespace Models\Base;

use Core\Model;

class PermissaoGrupoBase extends Model {

    /**
    * @var mixed $id;
    * @var mixed $grupo_id;
    * @var mixed $permissao_id;
    */

public function __construct ($data = [])
{
    $this->setTable('tblpermissao_grupo');
    $this->setPrimaryKey('id');
    parent::__construct($data);
}
}
