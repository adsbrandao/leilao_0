<?php

use Core\View;

$baseURL = \URL.'pagamento-config';

View::addBreadcrumb('Dashboard', URL.'admin/');
View::addBreadcrumb('Configurações de Pagamento', $baseURL);

View::setTitle('Configurações');

// View::useComponent('modals');
?>

<div class="ibox float-e-margins">
    <div class="ibox-content">
        <h1>Configuração de pagamento</h1>
    </div>
</div>