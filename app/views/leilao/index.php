<?php

use Core\View;
use Core\StringCase;

View::setTitle($title ?? 'Leilões');
?>
<div class="pull-right">
    <form id="frm_pesquisa" action="">
        <input type="hidden" name="p" id="f_page" value="<?=$page??0?>">
        <div class="pull-left">
            <input
                type="text"
                name="q"
                value="<?=$q??''?>"
                class="form-control"
                onchange="$('#frm_pesquisa').submit();"
                placeholder="Pesquisa">
        </div>
        <div class="pull-left">
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
        </div>
    </form>
</div>
<div class="clearfix"></div>
<div class="row">
    <?php while ($leilao = $data->fetch()) {?>
    <div class="col-md-3 col-xs-12">
        <a href="<?=\URL?>leilao/<?=$leilao->id?>">
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <div style="overflow:hidden;height: 200px;">
                    <?php 
                    if (is_null($leilao->imagem_thumb) || !is_file(\PATH.'upload/'.$leilao->imagem_thumb)) {
                        echo '<img src="'.\URL.'theme/img/logo.png" style="opacity:0.3;width: 100%;max-height: 200px;" />';
                    } else {
                        echo '<img src="'.\URL.'upload/'.$leilao->imagem_thumb.'" style="width: 100%;max-height: 200px;" />';
                    }
                    ?>
                    </div>
                    <h3><?=$leilao->nome?></h3>
                    <?php 
                    $listaStatus = [
                        ['label' => 'Não publicado', 'style' => 'default'],
                        ['label' => 'Publicado (Aberto)', 'style' => 'warning'],
                        ['label' => 'Em andamento', 'style' => 'success'],
                        ['label' => 'Encerrado', 'style' => 'danger'],
                        ['label' => 'Cancelado', 'style' => 'default'],
                        ['label' => 'Apregoado', 'style' => 'warning']
                    ];
                    $status = isset($listaStatus[$leilao->status])? $listaStatus[$leilao->status] : $listaStatus[0];
                    ?>

                    <div class="label label-<?=$status['style'];?>">
                        Lote
                        <?=$leilao->lote?>
                    </div>

                    <div style="margin-left:5px;" class="label label-<?=$status['style'];?>">
                        <?=$leilao->parcelado=='0' ? "À vista":"Parcelado"; ?>
                    </div>

                    <div style="margin-left:5px;" class="label label-<?=$status['style'];?>">
                        <?=$status['label'];?>
                    </div>
                    <div class="leilao-descricao" style="margin: 10px 0;"><?=$leilao->processo;?></div>
                    <div style="margin: 10px 0;font-size: 1.5em;display:flex;">
                        <div>Valor inicial:</div>
                        <div style="flex:1;text-align: right;">R$ <?=number_format($leilao->valor_inicial, 2, ',', '.');?></div>
                    </div>
                    <div class="btn btn-primary btn-block">Ver leilão</div>
                </div>
            </div>
        </a>
    </div>
    <?php }?>
</div>

<div>
    <?php
    $page = intval($page?? 0);
    $pages = intval($pages?? 0);

    $iInicial = $page - 5;
    if ($iInicial < 0) $iInicial = 0;

    $iFinal = $iInicial + 11;
    if ($iFinal >= $pages) $iFinal = $pages;

    $i = $iInicial;
    while ($i < $iFinal) {
        if ($page == $i) {
             echo '<button onclick="gotopage('.$i.');" class="btn btn-primary">'.($i+1).'</button> ';
        } else {
             echo '<button onclick="gotopage('.$i.');" class="btn btn-default">'.($i + 1).'</button> ';
        }
        $i++;
    }
    ?>
    <button class="btn btn-default"><?=$page+1?> de <?=$pages?> páginas.</button>
</div>

<script>
    function gotopage (page) {
        $('#f_page').val(page);
        $('#frm_pesquisa').submit();
    }
</script>