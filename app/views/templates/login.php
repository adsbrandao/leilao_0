<?php

use \Core\View;

$title = View::getSiteName();
$pageTitle = View::getTitle();

if (empty($title)) $title = 'Leilão';
if (!empty($pageTitle)) $title .= ' | ' . $pageTitle;
?>
<!DOCTYPE html>
<head>
 
    <!-- Metadata -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title?></title>
 
    <!-- CSS Files -->
    <link href="<?=URL?>theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=URL?>theme/font-awesome/css/font-awesome.css" rel="stylesheet">    
    <link href="<?=URL?>theme/css/animate.css" rel="stylesheet">
    <link href="<?=URL?>dist/style.css?v=2021" rel="stylesheet">
    <link href="<?=URL?>dist/custom.css?v=<?=\VERSION?>" rel="stylesheet">
</head>
<body class="gray-bg">
    <!-- Mainly scripts -->
    <script src="<?=URL;?>theme/js/jquery-3.1.1.min.js"></script>
    <script src="<?=URL;?>theme/js/bootstrap.min.js"></script>
    <script src="<?=URL;?>theme/js/plugins/mask/jquery.mask.min.js"></script>

    <script src="<?=URL;?>src/js/masks.js?v=<?=time()?>"></script>
    <script src="<?=URL;?>src/js/forms.js?v=<?=time()?>"></script>
    
    <?=$content??'';?>
</body>
</html>