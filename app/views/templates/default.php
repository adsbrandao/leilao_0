<?php

use \Core\View;

$title = View::getSiteName();
$pageTitle = View::getTitle();

if (empty($title)) $title = 'Leilão';
if (!empty($pageTitle)) $title .= ' | ' . $pageTitle;
?>
<!DOCTYPE html>
<head>
 
    <!-- Metadata -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$title?></title>
 
    <!-- CSS Files -->
    <link href="<?=URL?>theme/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=URL?>theme/font-awesome/css/font-awesome.css" rel="stylesheet">    
    <link href="<?=URL?>theme/css/animate.css" rel="stylesheet">
    <link href="<?=URL?>theme/css/plugins/dataTables/datatables.min.css" rel="stylesheet">    
    <link href="<?=URL?>theme/css/plugins/select2/select2.min.css" rel="stylesheet" type="text/css">
    <link href="<?=URL?>dist/style.css?v=2021" rel="stylesheet">
    <link href="<?=URL?>dist/custom.css?v=<?=\VERSION?>" rel="stylesheet">
</head>
<body>
    <!-- Mainly scripts -->
    <script>
        var baseURL = '<?=URL?>';
    </script>
    <script src="<?=URL;?>theme/js/jquery-3.1.1.min.js"></script>
    <script src="<?=URL;?>theme/js/bootstrap.min.js"></script>
    <script src="<?=URL;?>theme/js/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?=URL;?>theme/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Flot -->
    <script src="<?=URL;?>theme/js/plugins/flot/jquery.flot.js"></script>
    <script src="<?=URL;?>theme/js/plugins/flot/jquery.flot.tooltip.min.js"></script>
    <script src="<?=URL;?>theme/js/plugins/flot/jquery.flot.spline.js"></script>
    <script src="<?=URL;?>theme/js/plugins/flot/jquery.flot.resize.js"></script>
    <script src="<?=URL;?>theme/js/plugins/flot/jquery.flot.pie.js"></script>
    <script src="<?=URL;?>theme/js/plugins/flot/jquery.flot.symbol.js"></script>
    <script src="<?=URL;?>theme/js/plugins/flot/jquery.flot.time.js"></script>

    <!-- Peity -->
    <script src="<?=URL;?>theme/js/plugins/peity/jquery.peity.min.js"></script>
    <script src="<?=URL;?>theme/js/demo/peity-demo.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="<?=URL;?>theme/js/inspinia.js"></script>
    <script src="<?=URL;?>theme/js/plugins/pace/pace.min.js"></script>

    <!-- jQuery UI -->
    <script src="<?=URL;?>theme/js/plugins/jquery-ui/jquery-ui.min.js"></script>

    <!-- Jvectormap -->
    <script src="<?=URL;?>theme/js/plugins/jvectormap/jquery-jvectormap-2.0.2.min.js"></script>
    <script src="<?=URL;?>theme/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>

    <!-- EayPIE -->
    <script src="<?=URL;?>theme/js/plugins/easypiechart/jquery.easypiechart.js"></script>

    <!-- Sparkline -->
    <script src="<?=URL;?>theme/js/plugins/sparkline/jquery.sparkline.min.js"></script>

    <!-- Sparkline demo data  -->
    <script src="<?=URL;?>theme/js/demo/sparkline-demo.js"></script>

    <script src="<?=URL;?>theme/js/plugins/mask/jquery.mask.min.js"></script>

    <script src="<?=URL;?>src/js/masks.js?v=<?=time()?>"></script>
    <script src="<?=URL;?>src/js/forms.js?v=<?=time()?>"></script>
    <div id="wrapper">
        <?php include('menu-side.php');?>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <?php include('menu-header.php');?>
            </div>

            <div class="wrapper border-bottom white-bg page-heading" style="display: flex;margin: 0 -15px;padding: 0 10px 20px 10px;">
                <div style="flex: 1;padding: 0 15px;">
                    <h2><?=View::getTitle();?></h2>
                    <?php 
                        // breadcrumb ----------------------------
                        $breadcrumb = View::getBreadcrumb();
                        if (count($breadcrumb) > 0) {
                        $lastBreadcrumb = array_pop($breadcrumb);
                    ?>
                    <ol class="breadcrumb">
                    <?php foreach ($breadcrumb as $bread) { ?>
                        <li>
                            <a href="<?=$bread->href;?>"><?=$bread->title;?></a>
                        </li>
                    <?php } ?>

                        <li class="active">
                            <strong><?=$lastBreadcrumb->title;?></strong>
                        </li>
                    </ol>
                    <?php } 
                        // breadcrumb ----------------------------
                    ?>
                </div>
                <?php 
                $titleAction = View::getTitleAction();
                if (!empty($titleAction)) {
                ?>
                <div style="padding: 0 15px;">
                    <div class="title-action">
                        <?php
                        echo $titleAction
                         // <a href="" class="btn btn-primary">This is action area</a>
                        ?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="wrapper wrapper-content">
                <div class="row">
                    <div class="col-lg-12">
                        <?=$content??'';?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modais -->
</body>
</html>