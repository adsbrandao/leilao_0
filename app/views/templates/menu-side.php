<?php 

use Core\Auth;
use Core\Router;
use Models\MenuLateral;

$dataUser = Auth::get();

$isAuth = $dataUser != false;

$menus = MenuLateral::getData();

?>
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header" style="background: #F8F8F8;">
                    <?php if ($isAuth) { ?>
                    <div class="profile-element" style="text-align:center;">
                        <span>
                            <img
                                src="<?=URL?>theme/img/logo.png"
                                alt="Norte de Minas Leilões"
                                style="max-width: 95%;"
                                 />
                        </span>

                        <span class="clear">
                            <span class="block m-t-xs" style="color: #555;">
                                <strong class="font-bold"><?=$dataUser->nome;?></strong>
                                <a href="<?=URL;?>logout" style="color: #EA4646;">(Sair)</a>
                            </span>
                        </span>
                    </div>
                    <?php } ?>
                    <div class="logo-element">                        
                        <img
                                src="<?=URL?>theme/img/logo-icon.png"
                                alt="Norte de Minas Leilões"
                                style="max-width: 80%;" />
                    </div>
                </li>
                <?php 
                $requestURL = is_null(Router::$requestURL) ? '/' : Router::$requestURL;

                foreach ($menus as $item){
                    $clActive = Router::checkMatchUrl($item['caminho'], $requestURL)? ' class="active"': '';
                    if (count($item['filhos']) == 0) {
                        $href = URL . preg_replace('/^[\/]{0,1}(.*)$/', '$1', $item['caminho']);
                    } else {
                        $href = '#';
                    }
                    $html = '<li'.$clActive.'>';
                    $html .= '<a href="'.$href.'">';
                    $html .= '<i class="fa '.$item['icone'].'"></i> ';
                    $html .= '<span class="nav-label">'.$item['titulo'].'</span>';
                    if (count($item['filhos']) > 0) {
                        $html .= '<span class="fa arrow"></span>';
                        $html .= '</a>';
                        $html .= '<ul class="nav nav-second-level">';
                        foreach ($item['filhos'] as $subitem) {
                            $href = URL.preg_replace('/^[\/]{0,1}(.*)$/', '$1', $subitem['caminho']);
                            $html .= '<li><a href="'.$href.'">- '.$subitem['titulo'].'</a></li>';
                        }
                        $html .= '</ul>';
                    } else {
                        $html .= '</a>';
                    }
                    $html .= '</li>';

                    echo $html;
                }
                ?>
            </ul>

        </div>
    </nav>