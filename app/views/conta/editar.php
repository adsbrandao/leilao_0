<?php

use Core\View;

View::addBreadcrumb('Dashboard', URL.'/minha-area/');
View::addBreadcrumb('Minha conta', URL.'/minha-area/conta');

View::setTitle('Minha conta');
// View::addBreadcrumb('Ver', $baseURL);

?>


<div class="ibox float-e-margins">
    <div class="ibox-content">
        <div class="row">
                

<div class="tabs-container">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-1" aria-expanded="true"> Editar cadastro</a></li>
            </ul>

        <div class="ibox-content">
            <div id="dv_frm">
            <form id="frm_edit" action="#" method="post" enctype="multipart/form-data">
                <input type="hidden" value="<?=$data->tipo_pessoa;?>" name="tipo_pessoa" id="tipo_pessoa" value="0">
                <div class="row">                
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="nome">Nome</label>
                        <input
                            type="text"
                            value="<?=$data->nome;?>" name="nome"
                            id="nome"
                            required
                            pattern="[a-zA-Z0-9 ]{2}[a-zA-Z0-9 ]+"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="sobrenome">Sobrenome</label>
                        <input
                            type="text"
                            value="<?=$data->sobrenome;?>" name="sobrenome"
                            id="sobrenome"
                            required
                            pattern="[a-zA-Z0-9 ][a-zA-Z0-9 ]+"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="nascimento">Data de nascimento</label>
                        <input
                            type="date"
                            value="<?=$data->nascimento;?>" name="nascimento"
                            id="nascimento"
                            required
                            class="form-control">
                    </div>
                    <div
                        class="col-xs-12 col-md-3 form-group show-juridica"
                        style="display: none;">
                        <label for="razao_social">Razão Social</label>
                        <input
                            type="text"
                            value="<?=$data->razao_social;?>" name="razao_social"
                            id="razao_social"
                            pattern=".+" class="form-control">
                    </div>
                    <div
                        class="col-xs-12 col-md-3 form-group show-juridica"
                        style="display: none;">
                        <label for="cnpj">CNPJ</label>
                        <input
                            type="text"
                            value="<?=$data->cnpj;?>" name="cnpj"
                            id="cnpj"
                            pattern="[0-9]{2}[.][0-9]{3}[.][0-9]{3}[/][0-9]{4}[-][0-9]{2}"
                            placeholder="00.000.000/0000-00"
                            class="form-control cnpj">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="telefone">Telefone</label>
                        <input
                            type="text"
                            value="<?=$data->telefone;?>" name="telefone"
                            id="telefone"
                            placeholder="(00)0000-0000"
                            class="form-control telefone">
                    </div>
                    <div class="col-xs-12 col-md-3 form-group">
                        <label for="celular">Celular</label>
                        <input
                            type="text"
                            value="<?=$data->celular;?>" name="celular"
                            id="celular"
                            placeholder="(00)00000-0000"
                            required
                            pattern="[\(][0-9]{2}[\)][0-9]{5}[-][0-9]{4}"
                            class="form-control celular">
                    </div>
                </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="senha">Senha</label>
                        <input
                            type="password"
                            name="senha"
                            id="senha"
                            required
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="resenha">Confirme a senha</label>
                        <input
                            type="password"
                            name="resenha"
                            id="resenha"
                            required
                            class="form-control">
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12 col-md-2 form-group">
                        <label for="cep">CEP</label>
                        <input
                            type="text"
                            value="<?=$data->cep;?>" name="cep"
                            id="cep"
                            onchange="onChangeCep(this.value)"
                            required
                            pattern="[0-9]{5}[-][0-9]{3}"
                            class="form-control cep">
                    </div>
                    <div class="col-xs-12 col-md-6 form-group">
                        <label for="endereco">Endereço</label>
                        <input
                            type="text"
                            value="<?=$data->endereco;?>" name="endereco"
                            id="endereco"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-2 form-group">
                        <label for="numero">Número</label>
                        <input
                            type="text"
                            value="<?=$data->numero;?>" name="numero"
                            id="numero"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-2 form-group">
                        <label for="complemento">Complemento</label>
                        <input
                            type="text"
                            value="<?=$data->complemento;?>" name="complemento"
                            id="complemento"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="bairro">Bairro</label>
                        <input
                            type="text"
                            value="<?=$data->bairro;?>" name="bairro"
                            id="bairro"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="cidade">Cidade</label>
                        <input
                            type="text"
                            value="<?=$data->cidade;?>" name="cidade"
                            id="cidade"
                            class="form-control">
                    </div>
                    <div class="col-xs-12 col-md-4 form-group">
                        <label for="estado">Estado</label>
                        <input
                            type="text"
                            value="<?=$data->estado;?>" name="estado"
                            id="estado"
                            class="form-control">
                    </div>
                </div>
                <div id="dv_msg"></div>
                <div style="margin-top: 20px;">
                    <button
                        id="btn_salvar"
                        class="btn btn-primary m-t-n-xs"
                        type="submit">Salvar</button>
                </div>
            </form>
            </div>
            <div id="dv_mensagem" style="display:none;">
                <h1 class="text-primary">Você foi cadastrado com sucesso!</h1>
                <p>Mas ainda não está ativo em nossa plataforma,
                nossa equipe primeiro irá validar seus dados.</p>
                <a href="<?=URL;?>" class="btn btn-primary btn-lg">Voltar ao site</a>
            </div>
        </div>
    </div>
</div>

<script>
$('#btn_salvar').click(function (e) {
    $('#frm_edit').submit();
});

$('#frm_edit').submit(function (e) {
    e.preventDefault();

    $('#btn_salvar').addClass('disabled');
    $('#btn_salvar').prop('disabled', true);
    $('#btn_salvar').html(`<i class="fa fa-refresh fa-spin"></i> Salvar`);
    $('#dv_msg').html(`<span class="text-info">Aguarde.</span>`);

    let formData = new FormData(this);

    httpJSON('<?=URL;?>minha-area/conta', 'POST', formData)
        .then(res => {
            $('#btn_salvar').removeClass('disabled');
            $('#btn_salvar').prop('disabled', false);
            $('#btn_salvar').html(`Salvar`);
            if (res.status) {
                $('#dv_msg').html(`<span class="text-success">Salvo com sucesso</span>`);
                window.location = '<?=URL;?>minha-area';
            } else {
                $('#dv_msg').html(`<span class="text-danger">${res.message}</span>`);
                setTimeout(() => {
                    $('#dv_msg').html(``);
                }, 4000);
            }
        })
        .catch(reason => {
            $('#btn_salvar').removeClass('disabled');
            $('#btn_salvar').prop('disabled', false);
            $('#btn_salvar').html(`Salvar`);

            showMsg('#dv_msg', 'Erro ao salvar', 'fa fa-times', 'danger');
            $('#dv_msg').html(`<span class="text-danger">Erro ao salvar</span>`);
            setTimeout(() => {
                $('#dv_msg').html(``);
            }, 4000);
        });

});

</script>