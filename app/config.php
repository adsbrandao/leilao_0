<?php
date_default_timezone_set('America/Sao_Paulo');

// MODO: NORMAL ou DEBUG
if (!defined('MODO')) {
    define('MODO', 'NORMAL');
}


if (MODO == 'DEBUG') {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    define('VERSION', time());
} else {
    error_reporting(E_ERROR);
    ini_set('display_errors', 0);
    define('VERSION', '210304');
}

$servers = [
    'LOCAL' => [
        'localhost'
    ],
    'TEST' => [
        'sistema.webfinan.com.br',
        'www.sistema.webfinan.com.br'
    ],
    'PROD' => [
        'localhost:8080'
    ]
];
if (is_null($_SERVER['HTTP_HOST'])) {
    define('SERVER', 'LOCAL');
} else {
    // SERVER: LOCAL,TEST ou PROD
    foreach ($servers as $server => $hosts) {
        if (in_array($_SERVER['HTTP_HOST'], $hosts)) {
            define('SERVER', $server);
            break;
        }
    }
}

if (!defined('SERVER')) {
    throw new \Exception("Domínio'". $_SERVER['HTTP_HOST'] . "' não configurado!");
}

define('PRODUCTION', SERVER == 'PROD');
define('DEVELOPMENT', !PRODUCTION);

if (SERVER == 'PROD') {
    /* sistema.webfinan.com.br
    Leilao_NorteMinas
    leilao	
    leil@o@2021
    localhost:3306
    177.234.144.114*/
    // Config MYSQL
    define('DB_HOST', 'webfinan.com.br');
    define('DB_PORT', '3306');
    define('DB_NAME', 'Leilao_NorteMinas');
    define('DB_USER', 'leilao');
    define('DB_PASS', 'leil@o@2021');

    // URL e PATH
    define('PATH', '.' . DIRECTORY_SEPARATOR);
    define('URL', '/');
    define('URLSITE', 'http://localhost:8080/');
} elseif (SERVER == 'TEST') {
    // Config MYSQL
    define('DB_HOST', 'webfinan.com.br');
    define('DB_PORT', '3306');
    define('DB_NAME', 'Leilao_NorteMinas');
    define('DB_USER', 'leilao');
    define('DB_PASS', 'leil@o@2021');

    // URL e PATH
    define('PATH', realpath(__DIR__.'/..') . DIRECTORY_SEPARATOR);
    define('URL', '/norteminas/');
    define('URLSITE', 'https://sistema.webfinan.com.br/norteminas/');

    \Core\Router::$prefix = "/norteminas/";
} elseif (SERVER == 'LOCAL') {
    // Config MYSQL
    // define('DB_HOST', 'localhost');
    // define('DB_PORT', '3306');
    // define('DB_NAME', 'leilao_novo');
    // define('DB_USER', 'usuario');
    // define('DB_PASS', 'senha');

    define('DB_HOST', 'webfinan.com.br');
    define('DB_PORT', '3306');
    define('DB_NAME', 'Leilao_NorteMinas');
    define('DB_USER', 'leilao');
    define('DB_PASS', 'leil@o@2021');

    // URL e PATH
    define('PATH', realpath(__DIR__.'/..') . DIRECTORY_SEPARATOR);
    define('URL', '/leilao/');
    define('URLSITE', 'http://' . $_SERVER['HTTP_HOST']. '/');

    \Core\Router::$prefix = "/leilao/";
}

\Core\View::setSiteName('Leilão');
\Core\View::setTitle('');