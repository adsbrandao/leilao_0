<?php 

/**
 * Imprime o html do modal
 *
 * @param String $id
 * @param String $title
 * @param String $body
 * @param String $footer
 * @param String $classSize Classe para o tamanho do modal: modal-lg ou modal-sm ou vazio
 * @param String $animate Classe para animacao:  fadeIn flipInY fadeInRight bounceInRight
 * @return void
 */
function viewModal ($id, $title, $body = '', $footer = '', $classSize = '', $animate = '') { 
    if ($animate!='') {
        $animate = 'animated '.$animate;
    }
    ?>
<div class="modal inmodal fade" id="<?=$id?>" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog <?=$classSize?>">
        <div class="modal-content <?=$animate?>">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <?php if ($title!='') { ?>
                <h4 class="modal-title"><?=$title?></h4>
                <?php } ?>
            </div>
            <?php if ($body!='') { ?>
            <div class="modal-body">
                <?=$body?>
            </div>
            <?php } ?>

            <div class="modal-footer">
                <?=$footer?>
            </div>
        </div>
    </div>
</div>
<?php } // FIM viewModal

function viewModalFadeIn ($id, $title, $body = '', $footer = '', $classSize = '') {
    viewModal($id, $title, $body, $footer, $classSize, 'fadeIn');
} // FIM viewModalFadeIn

function viewModalFlipInY ($id, $title, $body, $footer = '', $classSize = '') {
    viewModal($id, $title, $body, $footer, $classSize, 'flipInY');
} // FIM viewModalflipInY

function viewModalFadeInRight($id, $title, $body, $footer = '', $classSize = '') {
    viewModal($id, $title, $body, $footer, $classSize, 'fadeInRight');
} // FIM viewModalFadeInRight

function viewModalBounceInRight($id, $title, $body, $footer = '', $classSize = '') {
    viewModal($id, $title, $body, $footer, $classSize, 'bounceInRight');
} // FIM viewModalBounceInRight

   
